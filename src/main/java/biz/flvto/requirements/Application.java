package biz.flvto.requirements;

import net.thucydides.core.annotations.Feature;

public class Application {

    @Feature
    public class Flv2mp3Org {
        public class RedirectTest {
        }

    }

    @Feature
    public class FlvToBiz {
        public class ConverterFunctional {
        }

        public class LinksTest {
        }

        public class BannerTest {
        }

        public class RobotTxtTest {
        }

        public class DownloadSoftwareTest {
        }

        public class ValidationTest {
        }

        public class LocalesTest {
        }

        public class MirrorsTest {
        }

        public class RedirectTest {
        }
    }

    @Feature
    public class ToConvCom {
        public class ConverterFunctional {
        }

        public class LinksTest {
        }

        public class BannerTest {
        }

        public class RobotTxtTest {
        }

        //		public class DownloadSoftwareTest{};  //TODO add after approve download page design
        public class ValidationTest {
        }

        public class MirrorsTest {
        }

        public class LocalesTest {
        }

        public class RedirectTest {
        }
    }

    @Feature
    public class MFlvToBiz {
        public class ConverterFunctional {
        }

        public class LinksTest {
        }

        public class BannerTest {
        }

        public class RobotTxtTest {
        }

        public class ValidationTest {
        }

        public class LocalesTest {
        }
    }

    @Feature
    public class Api {
        public class Coverter {
        }
    }

}
