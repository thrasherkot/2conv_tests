package biz.flvto.requirements;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

import net.serenitybdd.core.Serenity;

public class ParametersList {

    public static final String baseUrlFlvto = "http://www.flvto.biz";
    public static final String baseUrlFlv2Mp3 = "http://org.flv2mp3.org";
    public static final String baseUrlMFlv2Mp3 = "http://m.org.flv2mp3.org";
    public static final String baseUrl2conv = "http://2conv.com";
    public static final String youtubeUrl = "http://www.youtube.com/watch?v=gUzC5gqyaJo";
    public static final String downloadPath = "c:\\seleniumDownload\\";
    public static final String md5ExeFile = "ECE8C95506CFA989AE7F0D6B8984074D";
    public static final String md5DmgFile = "4CB648E5A58100831AC1FAF09D67F9A2";
    public static final String deletedVideoYoutubeUrl = "http://www.youtube.com/watch?v=h6XM6XORYPg";
    public static final Object[][] shouldBeValidUrls = {
            {"https://youtube.com/watch?v=1Zm6HzN5YVI&t=1m5s"},
            {"https://youtube.com/watch?v=1Zm6HzN5YVI&t=90s"},
            {"https://youtube.com/watch?v=1Zm6HzN5YVI"},
            {"https://www.youtube.com/watch?v=1Zm6HzN5YVI&list=RD1Zm6HzN5YVI"},
            {" http://youtu.be/1Zm6HzN5YVI "},
            {"http://youtu.be/1Zm6HzN5YVI "},
            {"https://youtu.be/1Zm6HzN5YVI#"},
            {"youtu.be/Jwgf3wmiA04"},
            {" www.youtube.com/watch?v=1Zm6HzN5YVI&list=RD1Zm6HzN5YVI"},
            {"https://m.youtube.com/watch?list=RDMq-aVCUs2Q0&params=OALAAQE%253D&v=Mq-aVCUs2Q0"},
            {" youtube.com/watch?v=1Zm6HzN5YVI&list=RD1Zm6HzN5YVI"},
            {"http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ"},
            {"https://www.youtube.com/v/LQoW9oDy3I4"},
            {"https://www.youtube.com/e/LQoW9oDy3I4"},
            {"https://www.youtube.com/embed/LQoW9oDy3I4"},
            {"http://www.youtube.com/watch?v=JCYIwiT0X98&feature=g-all-esi&context=G265c44fFAAAAAAAAHAA"},
            {"https://youtube.com/watch?v=1Zm6HzN5YVI#"},
            {"youtube.com/watch?v=1Zm6HzN5YVI#"},
            {"www.youtube.com/watch?v=1Zm6HzN5YVI#"},
            {"https://m.youtube.com/watch?v=_8iuWFo58BI&itct=CBoQpDAYACITCLC_lvKDgcsCFQVeHQodEq4DFDIHcmVsYXRlZEjT65Ge9fL979QB&client=mv-google&hl=en&gl=RU"},
            {"https://m.youtube.com/watch?v=3-VLG6RZfdIw"},
            {"m.youtube.com/watch?v=_8iuWFo58BI"},
            {"www.m.youtube.com/watch?v=_8iuWFo58BI"},
            {"http://www.m.youtube.com/watch?v=_8iuWFo58BI"},
            {"http://www.m.youtube.com/watch?v=_8iuWFo58BIasdqwef"},
    };

    public static final String localConvertIp = "192.168.0.211";

    public static Object[][] urlForConverter = {
            {"https://www.youtube.com/watch?v=0yuyWQceC9o", "mp3"},
            {"https://www.youtube.com/watch?v=wGey018JZaA", "mp4"},
            {"https://www.youtube.com/watch?v=ZgWcCyPecDY", "mp4 hd"},
            {"https://www.youtube.com/watch?v=6LHH_N4_ycI", "avi"},
            {"https://www.youtube.com/watch?v=zQWeDdnHJHI", "avi hd"}
    };

    public static String locales[] = {"it", "es", "fr", "de", "nl", "pt", "tr", "no", "kr", "jp", "pl", "cn", "hu", "ro", "gr", "cz", "bg", "sk", "rs", "ru", "en"};

    public static String userAgent = "", proxyHost, proxyUser, proxyPass;

    public static int proxyPort;

    public static void setUserAgent() {
        String tmp = System.getProperty("useragent");
        if (tmp != null) {
            switch (tmp) {
                case "mac":
                    userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/601.2.7 (KHTML, like Gecko) Version/9.0.1 Safari/601.2.7";
                    break;

                case "g531f":
                    userAgent = "Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-G531F Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.2 Chrome/38.0.2125.102 Mobile Safari/537.36";
                    break;

                case "galaxy":
                    userAgent = "Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-G920F Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.0 Chrome/38.0.2125.102 Mobile Safari/537.36";
                    break;

                case "ipad":
                    userAgent = "Mozilla/5.0 (iPad; CPU OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3";
                    break;

                case "iphone":
                    userAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3";
                    break;

                default:
                    userAgent = "";
                    break;
            }
        } else {
            userAgent = "";
        }
    }

    public static void setUserAgent(String ua) {
        userAgent = ua;
    }

    public static void getProxy() {
        proxyHost = "";
        proxyPort = 0;
        String tmp = System.getProperty("proxy");
        if (StringUtils.isNotEmpty(tmp)) {
            proxyHost = tmp;
            proxyPort = Integer.parseInt(System.getProperty("port"));
            proxyUser = System.getProperty("user");
            proxyPass = System.getProperty("pass");

        }
    }

    public static void setProfile() {
        if (!userAgent.equals("") && System.getProperty("webdriver.driver").equals("firefox")) {
            ProfilesIni allProfiles = new ProfilesIni();
            FirefoxProfile myProfile = allProfiles.getProfile("default");
            myProfile.setPreference("general.useragent.override", userAgent);
            Serenity.useFirefoxProfile(myProfile);
        }
    }

    public static boolean checkMobileTag() {
        String tmp = System.getProperty("tags");
        if (tmp != null) {
            switch (tmp) {
                case "MFlvTo":
                    return true;

                case "M2conv":
                    return true;

                default:
                    return false;
            }
        } else {
            return false;
        }
    }


}
