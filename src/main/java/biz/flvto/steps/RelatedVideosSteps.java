package biz.flvto.steps;

import biz.flvto.website.elements.AbstractRelatedVideosForm;
import biz.flvto.website.elements.flvtoElements.FlvtoRelatedVideos;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by admin on 29.01.2017.
 */
public class RelatedVideosSteps {

    private static RelatedVideosSteps instance;

    private AbstractRelatedVideosForm relatedVideosForm;

    private WebDriver driver;

    private RelatedVideosSteps(WebDriver driver, String siteName) {
        this.driver = driver;
        this.relatedVideosForm = getForm(siteName);
    }

    public AbstractRelatedVideosForm getForm(String formType) {
        switch (formType) {
            case "flvto":
                return new FlvtoRelatedVideos(driver, driver.findElement(By.cssSelector(FlvtoRelatedVideos.relatedFrameLocator)));

            default:
                return null;
        }
    }

    public static RelatedVideosSteps getRelatedVideosSteps(WebDriver driver, String siteName) {
        if (instance == null) {
            return new RelatedVideosSteps(driver, siteName);
        } else
            return instance;
    }

    public void convertRelatedVideo(int videoIndex, String format) {
        relatedVideosForm.convertRelatedVideo(videoIndex, format);
    }

}
