package biz.flvto.steps;

import biz.flvto.website.elements.AbstractMainForm;
import biz.flvto.website.elements.flvtoElements.FlvtoMainForm;
import biz.flvto.website.flvtobiz.MainPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by admin on 28.01.2017.
 */
public class MainFormConversionSteps {

    private static MainFormConversionSteps instance;

    private AbstractMainForm mainForm;

    private WebDriver driver;

    private MainFormConversionSteps(WebDriver driver, String formType){
        this.driver = driver;
        this.mainForm = getForm(formType);
    }


    public void type_youtube_link(String url){
        mainForm.typeUrl(url);
    }

    public void select_format(String format){
        mainForm.selectConversionFormat(format);
    }

    public void start_conversion(){
        mainForm.startConversion();
    }

    public void openConversionFormatList(){
        mainForm.openConversionFormatList();
    }

    public void checkCurrentFormatIs(String format){
        mainForm.checkCurrentFormatIs(format);
    }

    private AbstractMainForm getForm (String formType){
        switch (formType){
            case "flvto": return new FlvtoMainForm(driver, driver.findElement(By.id(MainPage.conversionFormLocator)));

            default: return null;
        }
    }

    public static MainFormConversionSteps getConversionSteps(WebDriver driver, String siteName){
        if (instance == null) {
            return new MainFormConversionSteps(driver, siteName);
        } else
            return instance;
    }

}
