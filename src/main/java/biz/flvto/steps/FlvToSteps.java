package biz.flvto.steps;

import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.thucydides.core.annotations.Step;

import net.thucydides.core.steps.ScenarioSteps;
import biz.flvto.requirements.ParametersList;
import biz.flvto.website.ChromeWithProxy;
import biz.flvto.website.DataWorker;
import biz.flvto.website.flvtobiz.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SuppressWarnings("serial")
public class FlvToSteps extends ScenarioSteps {

    MainPage mainPage;
    Progress progressPage;
    DownloadPage downloadPage;
    Top100 top100Page;
    DesktopConverterPage desktopConverterPage;
    Addons addonsPage;
    HowTo howtoPage;
    Support supportPage;
    TermsOfService termsPage;
    PrivacyPolicy policyPage;
    DMCA dmcaPage;
    FAQ faqPage;
    Advertisers advertisersPage;
    Set<String> oldWindowsSet;
    Robots robotsPage;
    DataWorker data;
    String currentUrl;
    Page404 page404;
    SoftwareAds softwareAdsPage;
    MainFormConversionSteps mainFormConversionSteps;
    RelatedVideosSteps relatedVideosSteps;
    boolean isMobile;


    @Step
    public void is_main_page_open() {
        getIndexPage("/en/");
        check_locale_main_page_open("/");
        stepInitiate();
    }

    @Step
    public void getIndexPage(String locale) {
        WebDriver driver = getDriver();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        try {
            getDriver().get(getPages().getDefaultBaseUrl() + locale);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step("open locale {0}")
    public void is_main_page_with_locale_open(String locale, String path) {
        getIndexPage(path);
        check_locale_main_page_open(path);
    }

    @Step
    public void check_main_page_banners() {
        mainPage.checkDesktopBanners();
        mainPage.checkMobileBanners();
    }

    @Step
    public void show_formats_button_click() {
        mainFormConversionSteps.openConversionFormatList();
    }


    @Step("Select {0} format")
    public void select_format(String format) {
        mainFormConversionSteps.select_format(format);
    }

    @Step("Check that {0} format selected")
    public void check_format_selected(String format) {
        mainFormConversionSteps.checkCurrentFormatIs(format);
    }

    @Step
    public void type_valid_youtube_link() {
        type_youtube_link(ParametersList.youtubeUrl);
    }

    @Step
    public void type_youtube_link(String url) {
        try {
            mainFormConversionSteps.type_youtube_link(url);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void convert_button_click() {
        try {
            mainFormConversionSteps.start_conversion();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void is_progress_page_open() {
        try {
            progressPage.waitForPorgressPage();
        } catch (TimeoutException e) {

        }
    }

    @Step
    public void wait_until_convertion_finish() {
        boolean flag = true;
        int downloadCounter = 0;
        int percentes = 0;
        while (flag) {
            try {
                percentes = progressPage.getProgressValue();
                downloadPage.waitForDonwloadButtonVisible();
                flag = false;
            } catch (TimeoutException e) {
                if (progressPage.isErrorFormVisible()) {
                    Assert.fail("Convertion error " + getDriver().getCurrentUrl());
                } else {
                    if (percentes == progressPage.getProgressValue()) {
                        downloadCounter++;
                    }
                }
                if (downloadCounter == 30) {
                    Assert.fail("Convertion timeout");
                }
            } catch (StaleElementReferenceException e) {
                flag = false;
            }
        }

    }

    @Step
    public void is_download_page_open() {
        getPages().currentPageAt(DownloadPage.class);
    }

    @Step
    public void check_progress_page_banners() {
        progressPage.checkDesktopBanners();
        progressPage.checkMobileBanners();
    }


    @Step
    public void check_download_page_banners() {
        downloadPage.checkDesktopBanners();
        downloadPage.checkMobileBanners();
    }


    private void mobile_menu_click() {
        if (isMobile) {
            mainPage.mobileMenuClick();
        }
    }

    @Step
    public void top100_header_link_click() {
        get_current_url_before_click();
        mobile_menu_click();
        mainPage.top100PageHeaderLinkClick();
    }

    @Step
    public void check_top100_page_banners() {
        top100Page.checkDesktopBanners();
        top100Page.checkMobileBanners();
    }

    @Step
    public void header_logo_click() {
        get_current_url_before_click();
        mainPage.mainPageLinkClick();
    }

    @Step
    public void desktop_converter_header_link_click() {
        get_current_url_before_click();
        mainPage.desktopConverterPageLinkClick();
    }

    @Step
    public void addons_header_link_click() {
        get_current_url_before_click();
        mainPage.addonsPageHeaderLinkClick();
    }

    @Step
    public void how_to_header_link_click() {
        get_current_url_before_click();
        mobile_menu_click();
        mainPage.howToPageHeaderLinkClick();
    }

    @Step
    public void support_header_link_click() {
        get_current_url_before_click();
        mobile_menu_click();
        mainPage.supportPageLinkClick();
    }

    @Step
    public void download_converter_tips_button_click() {
        get_windows_handles();
        mainPage.downloadSoftwareTipsButtonClick();
    }

    @Step
    public void download_app_for_windows_footer_link_click() {
        get_current_url_before_click();
        mainPage.downloadForWindowsPageLinkClick();
    }

    @Step
    public void download_app_for_mac_footer_link_click() {
        get_current_url_before_click();
        mainPage.downloadForMacPageLinkClick();
    }

    @Step
    public void download_app_for_win10_footer_link_click() {
        get_windows_handles();
        mainPage.downloadForWindows10PageLinkClick();
    }

    @Step
    public void download_app_for_win8_footer_link_click() {
        get_windows_handles();
        mainPage.downloadForWindows8PageLinkClick();
    }

    @Step
    public void get_windows_handles() {
        oldWindowsSet = getDriver().getWindowHandles();
    }

    @Step
    public void is_microsoft_store_page_open() {
        desktopConverterPage.switchToMicrosoftWindow(oldWindowsSet);
        new WebDriverWait(getDriver(), 30).until(ExpectedConditions.titleContains("YouTube"));
        String currentUrl = getDriver().getCurrentUrl();
        assertThat(currentUrl, containsString("microsoft"));
    }


    @Step
    public void terms_footer_link_click() {
        get_current_url_before_click();
        mainPage.termsFooterLinkClick();
    }

    @Step
    public void how_to_footer_link_click() {
        get_current_url_before_click();
        mainPage.howToFooterLinkClick();
    }

    @Step
    public void privacy_policy_footer_link_click() {
        get_current_url_before_click();
        mainPage.policyFooterLinkClick();
    }

    @Step
    public void dmca_footer_link_click() {
        get_current_url_before_click();
        mainPage.dmcaFooterLinkClick();
    }

    @Step
    public void feedback_footer_link_click() {
        get_current_url_before_click();
        mainPage.feedbackFooterLinkClick();
    }

    @Step
    public void faq_footer_link_click() {
        get_current_url_before_click();
        mainPage.faqFooterLinkClick();
    }

    @Step
    public void advertisers_footer_link_click() {
        get_current_url_before_click();
        mainPage.advertisersFooterLinkClick();
    }

    @Step
    public void top100_footer_link_click() {
        get_current_url_before_click();
        mainPage.top100FooterLinkClick();
    }

    @Step
    public void addons_footer_link_click() {
        get_current_url_before_click();
        mainPage.addonsFooterLinkClick();
    }

    @Step
    public void is_addons_page_open() {
        getPages().currentPageAt(Addons.class);
    }

    @Step
    public void is_advertisers_page_open() {
        getPages().currentPageAt(Advertisers.class);
    }

    @Step
    public void is_desktop_converter_page_open() {
        desktopConverterPage.waitForPageLoad();
        getPages().currentPageAt(DesktopConverterPage.class);
    }

    @Step
    public void is_dmca_page_open() {
        getPages().currentPageAt(DMCA.class);
    }

    @Step
    public void is_faq_page_open() {
        getPages().currentPageAt(FAQ.class);
    }

    @Step
    public void is_how_to_page_open() {
        getPages().currentPageAt(HowTo.class);
    }

    @Step
    public void is_privacy_policy_page_open() {
        getPages().currentPageAt(PrivacyPolicy.class);
    }

    @Step
    public void is_support_page_open() {
        getPages().currentPageAt(Support.class);
    }

    @Step
    public void is_terms_of_service_page_open() {
        getPages().currentPageAt(TermsOfService.class);
    }

    @Step
    public void check_desktop_converter_page_elements_visible() {
        desktopConverterPage.checkDownloadButtonVisible();
    }

    @Step
    public void check_elements_at_addons_page_availible() {
        addonsPage.checkFirefoxLinkAvailible();
        addonsPage.checkChromeLinkAvailible();
        addonsPage.checkIeLinkAvailible();
        addonsPage.checkSafariLinkAvailible();
    }

    @Step
    public void check_elements_at_how_to_page() {
        int linksCounter = 36;                        //���������� ������ �� ��������
        howtoPage.checkLinksCounter(linksCounter);
    }

    @Step
    public void check_elements_at_support_page() {
        supportPage.isSendButtonVisible();
    }

    @Step
    public void is_top100_page_open() {
        getPages().currentPageAt(Top100.class);
    }

    @Step
    public void cancel_convertation_button_click() {
        progressPage.cancelConvertationButtonClick();
    }

    @Step
    public void accept_allert() {
        data.acceptAllert();
    }

    @Step
    public void check_elements_at_terms_page() {
        int headersCounter = 1;        //���������� ���������� �� ��������
        termsPage.checkHeadersCounter(headersCounter);
    }

    @Step
    public void check_elements_at_privacy_page() {
        int headersCounter = 1;
        policyPage.checkHeadersCounter(headersCounter);
    }

    @Step
    public void check_elements_at_dmca_page() {
        dmcaPage.checkSendButtonVisible();
    }

    @Step
    public void check_elements_at_faq_page() {
        int headersCounter = 11;        //���������� ���������� �� ��������
        faqPage.checkHeadersCounter(headersCounter);
    }

    @Step
    public void check_elements_at_advertisers_page() {
        advertisersPage.checkSendButtonVisible();
    }

    @Step
    public void check_elements_at_top100_page_visible() {
        top100Page.checkShowFormatButtonVisible();
    }

    @Step
    public void check_elements_at_download_app_for_mac_page() {
        desktopConverterPage.checkModalDialogVisible();
    }

    @Step
    public void robots_file_open() {
        getDriver().get(getPages().getDefaultBaseUrl() + "/robots.txt");
    }

    @Step
    public void is_robots_file_open() {
        getPages().currentPageAt(Robots.class);
    }

    @Step
    public void check_robots_file() {
        robotsPage.checkRobots();
    }

    @Step
    public void wait_for_page_change() {
        boolean isChanged = false;
        while (!isChanged) {
            isChanged = data.waitingForPageChanged(currentUrl);
        }

    }

    @Step
    public void related_video_convert(String format) {
        get_windows_handles();
        relatedVideosSteps = RelatedVideosSteps.getRelatedVideosSteps(getDriver(),getSiteName());
        related_video_convert(0, format);
    }

    @Step("Convert related video with index {0} to {1} format")
    public void related_video_convert(int index, String format) {
        relatedVideosSteps.convertRelatedVideo(index,format);
    }

    @Step
    public void switch_to_new_window() {
        try {
            data.switchToNewWindow(oldWindowsSet);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void send_link_to_email_click() {
        try {
            downloadPage.sendToEmailLinkClick();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void type_email_to_send() {
        downloadPage.typeEmail("mikhail@hotger.com");
    }

    @Step
    public void send_email_button_click() {
        downloadPage.sendToEmailButtonClick();
    }

    @Step
    public void check_email_from_download_sended() {
        downloadPage.checkEmailSuccessfullSended();
    }

    @Step
    public void top100_page_open() {
        getDriver().get(getPages().getDefaultBaseUrl() + "/top100");
    }

    @Step
    public void get_current_url_before_click() {
        currentUrl = getDriver().getCurrentUrl();
    }

    @Step
    public void download_app_button_click() {
        desktopConverterPage.downloadAppButtonClick();
    }

    @Step
    public void wait_for_file_download() {
        boolean finished = false;
        int i = 0;
        while (!finished) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            File downloadPath = new File(ParametersList.downloadPath);
            for (File f : downloadPath.listFiles()) {
                if (f.getName().contains("part")) {
                    finished = false;
                    break;
                } else finished = true;
            }
            i++;
            if (i > 180) Assert.fail("Download timeout");
        }
    }

    @Step
    public void check_the_downloaded_file(String os) {
        desktopConverterPage.verifyFileChecksumm(os);
    }

    @Step
    public void download_app_index_button_click() {
        get_current_url_before_click();
        mainPage.downloadAppButtonClick();
    }

    @Step
    public void is_index_error_popup_visible() {
        mainPage.checkErrorPopupVisible();
    }

    @Step
    public void index_popup_download_button_click() {
        mainPage.popupDownloadButtonClick();
    }

    @Step
    public void type_deleted_video_url() {
        mainFormConversionSteps.type_youtube_link(ParametersList.deletedVideoYoutubeUrl);
    }

    @Step
    public void set_minute_for_waits() {
        progressPage.setImplicitTimeout(60, TimeUnit.SECONDS);
    }

    @Step
    public void wait_for_progress_error_popup() {
        set_minute_for_waits();
        progressPage.waitForPopupError();
    }

    @Step
    public void progress_popup_download_button_click() {
        progressPage.popupDownloadButtonClick();
    }

    @Step
    public void download_using_app_result_page_button_click() {
        downloadPage.downloadUsingAppButtonClick();
    }

    @Step
    public void get_404_page() {
        getDriver().get(getPages().getDefaultBaseUrl() + "/123");
    }

    @Step
    public void is_404_page_open() {
        page404.checkDownloadButtonVisible();
    }

    @Step
    public void page_404_download_button_click() {
        get_current_url_before_click();
        page404.downloadButtonClick();
    }

    @Step
    public void download_app_for_mac_link_click() {
        get_current_url_before_click();
        desktopConverterPage.downloadAppForMacLinkClick();
    }

    @Step
    public void is_download_app_for_mac_popup_visible() {
        desktopConverterPage.checkModalDialogVisible();
    }

    @Step
    public void close_popup_at_desktop_converter_page() {
        desktopConverterPage.closePopupButtonClick();
    }

    @Step
    public void download_popup_button_desktop_converter_page_click() {
        desktopConverterPage.downloadPopupButtonClick();
    }


    private void stop() {
        try {
            ((JavascriptExecutor) getDriver()).executeScript("window.stop();");
        } catch (WebDriverException e) {

        }
    }


    @Step("checking url {0}")
    public void validate_url(String url) {
        type_youtube_link(url);
        convert_button_click();
        is_progress_page_open();
    }

    @Step
    public void win10_landing_open() {
        getDriver().get(getPages().getDefaultBaseUrl() + "/youtube-downloader-for-win10");
    }

    @Step
    public void check_win10_landing_open() {
        desktopConverterPage.checkForWin10LandingDownloadButton();
    }

    @Step
    public void check_locale_main_page_open(String path) {
        String checkingUrl;
        if (path.equals("/en/")) {
            checkingUrl = getPages().getDefaultBaseUrl().replace("www.", "") + "/";
        } else {
            checkingUrl = getPages().getDefaultBaseUrl().replace("www.", "") + path;
        }
        String currentUrl = getDriver().getCurrentUrl().replace("www.", "");
        assertThat(currentUrl, is(checkingUrl));
    }

    @Step
    public void youtube_to_mp3_mirror_link_click() {
        mainPage.youtubeToMp3LinkClick();
    }

    @Step
    public void youtube_to_mp4_mirror_link_click() {
        mainPage.youtubeToMp4LinkClick();
    }

    @Step
    public void youtube_to_mp4_hd_mirror_link_click() {
        mainPage.youtubeToMp4HDLinkClick();
    }

    @Step
    public void youtube_to_avi_mirror_link_click() {
        mainPage.youtubeToAviLinkClick();
    }

    @Step
    public void youtube_to_avi_hd_mirror_link_click() {
        mainPage.youtubeToAviHDLinkClick();
    }

    @Step
    public void check_current_url(String url) {
        String currentUrl = getDriver().getCurrentUrl();
        assertThat(currentUrl, containsString(url));
    }

    @Step
    public void check_download_page_banners_with_locales() {
        check_download_page_banners();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_download_page_banners();
        }
    }

    @Step
    public void language_selector_click(boolean oldDesign) {
        mainPage.languageSelectorClick(oldDesign);
    }

    @Step("select language {0}")
    public void select_language(String lang) {
        try {
            mainPage.selectLanguage(lang);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void maximize_window() {
        getDriver().manage().window().maximize();
    }

    @Step
    public void check_main_page_banners_with_locales() {
        check_main_page_banners();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_locale_main_page_open("/" + ParametersList.locales[i] + "/");
            check_main_page_banners();
        }
    }

    @Step
    public void check_top100_page_banners_with_locales() {
        check_top100_page_banners();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_top100_page_banners();
        }
    }

    @Step
    public void check_desktop_converter_page_with_locale() {
        check_desktop_converter_page_elements_visible();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_desktop_converter_page_elements_visible();
        }
    }

    @Step
    public void check_elements_at_addons_page_availible_with_locale() {
        check_elements_at_addons_page_availible();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(true);
            select_language(ParametersList.locales[i]);
            check_elements_at_addons_page_availible();
        }
    }

    @Step
    public void check_elements_at_how_to_page_with_locale() {
        check_elements_at_how_to_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_how_to_page();
        }
    }

    @Step
    public void check_elements_at_support_page_with_locale() {
        check_elements_at_support_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_support_page();
        }
    }

    @Step
    public void check_elements_at_terms_page_with_locale() {
        check_elements_at_terms_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_terms_page();
        }
    }

    @Step
    public void check_elements_at_privacy_page_with_locale() {
        check_elements_at_privacy_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_privacy_page();
        }
    }

    @Step
    public void check_elements_at_dmca_page_with_locale() {
        check_elements_at_dmca_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_dmca_page();
        }
    }

    @Step
    public void check_elements_at_faq_page_with_locale() {
        check_elements_at_faq_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_faq_page();
        }
    }

    @Step
    public void check_elements_at_advertisers_page_with_locale() {
        check_elements_at_advertisers_page();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click(false);
            select_language(ParametersList.locales[i]);
            check_elements_at_advertisers_page();
        }
    }

    private void localesCycle(WebElement element, String title, boolean oldDesign) {
        for (int i = 0; i < ParametersList.locales.length; i++) {
            get_current_url_before_click();
            language_selector_click(oldDesign);
            select_language(ParametersList.locales[i]);
            wait_for_page_change();
            checkUrl(ParametersList.locales[i] + "/" + title);
            data.checkElementVisible(element);
        }
    }

    private void checkUrl(String url) {
        String checkingUrl = url.replace("en/", "/");
        assertThat(getDriver().getCurrentUrl(), containsString(checkingUrl));
    }

    @Step
    public void stop_convertion() {
        data.stopConvertion();
    }

    @Step
    public void check_direct_link() {
        String page = getDriver().getCurrentUrl();
        ChromeWithProxy.proxy.newHar(page);
        downloadPage.downloadButtonClick();
        data.checkDownloadFileResponseFromApi();
        ChromeWithProxy.proxy.stop();
    }

    @Step
    public void software_ads_page_open() {
        String page = getPages().getDefaultBaseUrl() + "/software_ads/";
        ChromeWithProxy.proxy.newHar(page);
        getDriver().get(page);
        softwareAdsPage.checkPageLoadStatus();
        ChromeWithProxy.proxy.stop();
    }

    @Step
    public void check_software_ads_banner() {
        softwareAdsPage.checkBanner();
        softwareAdsPage.checkOnlyOneBannerZoneHere();
    }

    @Step
    public void open_page(String url) {
        getDriver().get(getPages().getDefaultBaseUrl() + "/" + url);
        check_current_url(url);
    }

    @Step
    public void check_index_mirror_with_locale(String url) {
        data.checkElementVisible(mainPage.downloadAppButton);
        localesCycle(mainPage.downloadAppButton, url, false);
    }

    @Step
    public void check_page_with_locale(String url) {
        data.checkElementVisible(mainPage.mirrorImage);
        localesCycle(mainPage.mirrorImage, url, true);
    }

    @Step
    public void check_elements_at_top100_page_visible_with_locale() {
        check_elements_at_top100_page_visible();
        localesCycle(top100Page.showFormatButton, "top100/", false);
    }

    @Step
    public void check_page_from_software(String string) {
        assertThat(getDriver().getCurrentUrl(), containsString(string));
        data.checkElementVisible(getDriver().findElement(By.cssSelector("a.download-btn")));
    }

    @Step
    public void wait_a_minute() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }

    @Step
    public void check_redirect() {
        ChromeWithProxy.proxy.stop();
        data.market_search(getPages().getDefaultBaseUrl());
    }

    @Step
    public void set_window_size(int width, int height) {
        getDriver().manage().window().setSize(new Dimension(width, height));
    }

    @Step
    public void download_converted_video_button_click() {
        try {
            downloadPage.downloadButtonClick();
        } catch (TimeoutException e) {

        }
    }

    @Step
    public void start_request_capture(String page) {
        ChromeWithProxy.proxy.newHar();
    }

    @Step
    public void set_window_size(boolean isMobile) {
        if (isMobile) {
            set_window_size(540, 960);
        } else {
            maximize_window();
        }
        this.isMobile = isMobile;
    }

    private void stepInitiate() {
        String siteName = getSiteName();
        mainFormConversionSteps = MainFormConversionSteps.getConversionSteps(getDriver(), siteName);
    }

    private String getSiteName(){
        String siteName = getPages().getDefaultBaseUrl().replace("http://","").replace("www.","");
        return  siteName.split("\\.")[0];
    }

}
