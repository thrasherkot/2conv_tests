package biz.flvto.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import biz.flvto.requirements.ParametersList;
import biz.flvto.website.ChromeWithProxy;
import biz.flvto.website.DataWorker;
import biz.flvto.website.toconv.*;

@SuppressWarnings("serial")
public class ToConvSteps extends ScenarioSteps {

    MainPage mainPage;
    Progress progressPage;
    Download downloadPage;
    Related related;
    DataWorker data;
    Set<String> oldWindowsSet;
    Robots robotsPage;
    String currentUrl;
    DesktopConverter desktopConverterPage;
    Support supportPage;
    InfoPages infoPage;
    Advertisers advertisersPage;
    boolean isMobile;

    @Step
    public void is_main_page_open() {
        getIndexPage("/en/");
        check_locale_main_page_open("/");
    }

    @Step
    public void getIndexPage(String locale) {
        WebDriver driver = getDriver();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        try {
            getDriver().get(getPages().getDefaultBaseUrl() + locale);
        } catch (TimeoutException e) {
            stop();
        }
    }

    private void stop() {
        try {
            ((JavascriptExecutor) getDriver()).executeScript("window.stop();");
        } catch (WebDriverException e) {

        }
    }

    @Step("open locale {0}")
    public void is_main_page_with_locale_open(String locale, String path) {
        getIndexPage(path);
        check_locale_main_page_open(path);
    }

    @Step
    public void check_main_page_banners() {
        mainPage.checkDesktopBanners();
        mainPage.checkMobileBanners();
    }

    @Step
    public void type_valid_youtube_link() {
        type_youtube_link(ParametersList.youtubeUrl);
    }

    @Step
    public void type_youtube_link(String url) {
        mainPage.typeUrl(url);
    }

    @Step
    public void convert_button_click() {
        try {
            mainPage.convertButtonClick();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void is_progress_page_open() {
        try {
            progressPage.waitForPorgressPage();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void wait_until_convertion_finish() {
        boolean flag = true;
        int downloadCounter = 0;
        int percentes = 0;
        while (flag) {
            try {
                percentes = progressPage.getProgressValue();
                downloadPage.waitForDonwloadButtonVisible();
                flag = false;
            } catch (TimeoutException e) {
                if (progressPage.isErrorFormVisible()) {
                    Assert.fail("Convertion error " + getDriver().getCurrentUrl());
                } else {
                    if (percentes == progressPage.getProgressValue()) {
                        downloadCounter++;
                    }
                }
                if (downloadCounter == 10) {
                    Assert.fail("Convertion timeout");
                }
            } catch (StaleElementReferenceException e) {
                flag = false;
            }
        }

    }

    @Step
    public void is_download_page_open() {
        getPages().currentPageAt(Download.class);
    }

    @Step
    public void check_progress_page_banners() {
        progressPage.checkDesktopBanners();
        progressPage.checkMobileBanners();
    }


    @Step
    public void check_download_page_banners() {
        downloadPage.checkDesktopBanners();
        downloadPage.checkMobileBanners();
    }

    @Step
    public void show_formats_button_click() {
        mainPage.showFormatButtonClick();
    }


    @Step
    public void select_mp3_format() {
        show_formats_button_click();
        mainPage.mp3SelectButtonClick();
    }

    @Step
    public void check_mp3_format_selected() {
        mainPage.checkCurrentFormatIs("mp3");
    }

    @Step
    public void select_mp4_format() {
        show_formats_button_click();
        mainPage.mp4SelectButtonClick();
    }

    @Step
    public void check_mp4_format_selected() {
        mainPage.checkCurrentFormatIs("mp4");
    }

    @Step
    public void select_mp4_hd_format() {
        show_formats_button_click();
        mainPage.mp4HDSelectButtonClick();
    }

    @Step
    public void check_mp4_hd_format_selected() {
        mainPage.checkCurrentFormatIs("mp4 hd");
    }

    @Step
    public void select_avi_format() {
        show_formats_button_click();
        mainPage.aviSelectButtonClick();
    }

    @Step
    public void check_avi_format_selected() {
        mainPage.checkCurrentFormatIs("avi");
    }

    @Step
    public void select_avi_hd_format() {
        show_formats_button_click();
        mainPage.aviHDSelectButtonClick();
    }

    @Step
    public void check_avi_hd_format_selected() {
        mainPage.checkCurrentFormatIs("avi hd");
    }

    @Step
    public void related_show_format_button_click() {
        try {
            related.showFormatButtonClick();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void related_mp3_format_select() {
        related.mp3SelectButtonClick();
    }

    @Step
    public void related_mp4_format_select() {
        related.mp4SelectButtonClick();
    }

    @Step
    public void related_mp4_hd_format_select() {
        related.mp4HDSelectButtonClick();
    }

    @Step
    public void related_avi_format_select() {
        related.aviSelectButtonClick();
    }

    @Step
    public void related_avi_hd_format_select() {
        related.aviHDSelectButtonClick();
    }

    @Step
    public void related_convert_button_click() {
        get_windows_handles();
        related.convertButtonClick();
    }

    @Step
    public void switch_to_new_window() {
        try {
            data.switchToNewWindow(oldWindowsSet);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void switch_to_related_frame() {
        mainPage.swithToRelatedFrame();
    }

    @Step
    public void get_windows_handles() {
        oldWindowsSet = getDriver().getWindowHandles();
    }

    @Step("open locale {0}")
    public void open_main_page_with_local(String localeName, String localPath) {
        getIndexPage(localPath);
    }

    @Step
    public void check_locale_main_page_open(String path) {
        String checkingUrl;
        if (path.equals("/en/")) {
            checkingUrl = getPages().getDefaultBaseUrl() + "/";
        } else {
            checkingUrl = getPages().getDefaultBaseUrl() + path;
        }
        String currentUrl = getDriver().getCurrentUrl().replace("www.", "");
        assertThat(currentUrl, is(checkingUrl));
    }

    @Step
    public void robots_file_open() {
        getDriver().get(getPages().getDefaultBaseUrl() + "/robots.txt");
    }

    @Step
    public void is_robots_file_open() {
        getPages().currentPageAt(Robots.class);
    }

    @Step
    public void check_robots_file() {
        robotsPage.checkRobots();
    }

    @Step("checking url {0}")
    public void validate_url(String url) {
        type_youtube_link(url);
        convert_button_click();
        is_progress_page_open();
    }

    @Step
    public void get_current_url_before_click() {
        currentUrl = getDriver().getCurrentUrl();
    }

    @Step
    public void wait_for_page_change() {
        boolean isChanged = false;
        while (!isChanged) {
            isChanged = data.waitingForPageChanged(currentUrl);
        }

    }

    @Step
    public void desktop_converter_header_link_click() {
        get_windows_handles();
        mainPage.headerDownloadConverterLinkClick();
    }

    @Step
    public void is_desktop_converter_page_open() {
        getPages().currentPageAt(DesktopConverter.class);
    }

    @Step
    public void check_download_button_at_desktop_converter_page_visible() {
        desktopConverterPage.checkDownloadButtonVisible();
    }

    @Step
    public void support_header_link_click() {
        mobile_menu_click();
        mainPage.headerSupportLinkClick();
    }

    @Step
    public void is_support_page_open() {
        getPages().currentPageAt(Support.class);
    }

    @Step
    public void check_elements_at_support_page() {
        supportPage.isElementsVisible();
    }

    @Step
    public void download_converter_button_click() {
        get_windows_handles();
        mainPage.downloadConverterButtonClick();
    }

    @Step
    public void terms_footer_link_click() {
        mainPage.footerTermsLinkClick();
    }

    @Step
    public void is_terms_of_service_page_open() {
        checkUrl("/terms");
    }

    private void checkUrl(String url) {
        String checkingUrl = url.replace("en/", "/");
        assertThat(getDriver().getCurrentUrl(), containsString(checkingUrl));
    }

    @Step
    public void check_info_page_header() {
        infoPage.isMainLabelVisible();
    }

    @Step
    public void privacy_policy_footer_link_click() {
        mainPage.footerPolicyLinkClick();
    }

    @Step
    public void is_privacy_policy_page_open() {
        checkUrl("/policy");
    }

    @Step
    public void report_abuse_footer_link_click() {
        mainPage.footerReportAbuseLinkClick();
    }

    @Step
    public void feedback_footer_link_click() {
        mainPage.footerFeedbackLinkClick();
    }

    @Step
    public void faq_footer_link_click() {
        mainPage.footerFaqLinkCLick();
    }

    @Step
    public void is_faq_page_open() {
        checkUrl("/faq");
    }

    @Step
    public void check_elements_at_faq_page() {
        check_info_page_header();
        infoPage.checkHeadersCounter(11); // 11 ������������� � faq
    }

    @Step
    public void advertisers_footer_link_click() {
        mainPage.footerAdvertisersLinkCLick();
    }

    @Step
    public void is_advertisers_page_open() {
        getPages().currentPageAt(Advertisers.class);
    }

    @Step
    public void check_elements_at_advertisers_page() {
        advertisersPage.isElementsVisible();
    }

    @Step
    public void maximize_window() {
        getDriver().manage().window().maximize();
    }

    @Step
    public void youtube_to_mp3_mirror_link_click() {
        mainPage.youtubeToMP3LinkClick();
    }

    @Step
    public void youtube_to_mp4_mirror_link_click() {
        mainPage.youtubeToMP4LinkClick();
    }

    @Step
    public void youtube_to_avi_mirror_link_click() {
        mainPage.youtubeToAviLinkClick();
    }

    @Step
    public void youtube_video_downloader_mirror_link_click() {
        mainPage.youtubeVideoDownloaderLinkClick();
    }

    @Step
    public void check_mirror_url(String url) {
        String currentUrl = getDriver().getCurrentUrl();
        assertThat(currentUrl, containsString(url));
    }

    @Step
    public void language_selector_click() {
        mainPage.languageSwitcherClick();
    }

    @Step("select language {0}")
    public void select_language(String lang) {
        try {
            mainPage.selectLanguage(lang);
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void check_main_page_banners_with_locale() {
        check_main_page_banners();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click();
            select_language(ParametersList.locales[i]);
            check_main_page_banners();
        }
    }

    @Step
    public void check_download_page_banners_with_locale() {
        check_download_page_banners();
        for (int i = 0; i < ParametersList.locales.length; i++) {
            language_selector_click();
            select_language(ParametersList.locales[i]);
            check_download_page_banners();
        }
    }

    private void localesCycle(WebElement element, String title) {
        for (int i = 0; i < ParametersList.locales.length; i++) {
            get_current_url_before_click();
            language_selector_click();
            select_language(ParametersList.locales[i]);
            wait_for_page_change();
            checkUrl(ParametersList.locales[i] + "/" + title);
            data.checkElementVisible(element);
        }
    }

    @Step
    public void check_desktop_converter_page_elements_with_locale() {
        check_download_button_at_desktop_converter_page_visible();
        localesCycle(desktopConverterPage.downloadButton, "youtube-downloader");
    }

    @Step
    public void check_elements_at_support_page_with_locale() {
        check_elements_at_support_page();
        localesCycle(supportPage.sendFeedbackButton, "feedback");
    }

    @Step
    public void check_info_page_header_with_locale(String title) {
        check_info_page_header();
        localesCycle(infoPage.mainHeader, title);
    }

    @Step
    public void check_elements_at_advertisers_page_with_locale() {
        check_elements_at_advertisers_page();
        localesCycle(advertisersPage.sendButton, "advertisers");
    }

    @Step
    public void stop_convertion() {
        data.stopConvertion();
    }

    @Step
    public void header_index_link_click() {
        mainPage.headerMainPageLinkClick();
    }

    @Step
    public void wait_a_minute() {
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }

    @Step
    public void check_redirect() {
        ChromeWithProxy.proxy.stop();
        data.market_search(getPages().getDefaultBaseUrl());
    }

    @Step
    public void set_window_size(int width, int height) {
        getDriver().manage().window().setSize(new Dimension(width, height));
    }

    @Step
    public void download_converted_video_button_click() {
        try {
            downloadPage.downloadButtonClick();
        } catch (TimeoutException e) {

        }
    }

    @Step
    public void set_window_size(boolean isMobile) {
        if (isMobile) {
            set_window_size(540, 960);
        } else {
            maximize_window();
        }
        this.isMobile = isMobile;
    }

    private void mobile_menu_click() {
        if (isMobile) {
            mainPage.mobileMenuClick();
        }
    }


}
