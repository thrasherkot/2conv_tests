package biz.flvto.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import biz.flvto.requirements.ParametersList;
import biz.flvto.website.ChromeWithProxy;
import biz.flvto.website.DataWorker;
import biz.flvto.website.flv2mp3.*;

@SuppressWarnings("serial")
public class Flv2Mp3Steps extends ScenarioSteps {

    MainPage mainPage;
    Progress progressPage;
    DownloadPage downloadPage;
    DataWorker data;
    String currentUrl;
    boolean mobilePage = false;

    @Step
    public void is_main_page_open(boolean isMobile) {
        if (isMobile) {
            mobilePage = true;
            getPages().setDefaultBaseUrl(ParametersList.baseUrlMFlv2Mp3);
        }
        getIndexPage("/en/");
        check_locale_main_page_open("/");
    }

    @Step
    public void getIndexPage(String locale) {
        WebDriver driver = getDriver();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
        try {
            getDriver().get(getPages().getDefaultBaseUrl() + locale);
        } catch (TimeoutException e) {
            stop();
        }
    }

    private void stop() {
        try {
            ((JavascriptExecutor) getDriver()).executeScript("window.stop();");
        } catch (WebDriverException e) {

        }
    }

    @Step("open locale {0}")
    public void is_main_page_with_locale_open(String locale, String path) {
        getIndexPage(path);
        check_locale_main_page_open(path);
    }

    @Step
    public void check_main_page_banners() {
        mainPage.checkDesktopBanners();
        mainPage.checkMobileBanners();
    }

    @Step
    public void type_valid_youtube_link() {
        type_youtube_link(ParametersList.youtubeUrl);
    }

    @Step
    public void type_youtube_link(String url) {
        mainPage.typeYoutubeLink(url);
    }

    @Step
    public void convert_button_click() {
        try {
            if (mobilePage) {
                mainPage.mobileConvertButtonClick();
            } else {
                mainPage.convertButtonClick();
            }
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void is_progress_page_open() {
        try {
            progressPage.waitForPorgressPage();
        } catch (TimeoutException e) {
            stop();
        }
    }

    @Step
    public void wait_until_convertion_finish() {
        boolean flag = true;
        int downloadCounter = 0;
        int percentes = 0;
        while (flag) {
            try {
                percentes = progressPage.getProgressValue();
                downloadPage.waitForDownloadButtonVisible(mobilePage);
                flag = false;
            } catch (TimeoutException e) {
                if (progressPage.isErrorFormVisible()) {
                    Assert.fail("Convertion error " + getDriver().getCurrentUrl());
                } else {
                    if (percentes == progressPage.getProgressValue()) {
                        downloadCounter++;
                    }
                }
                if (downloadCounter == 10) {
                    Assert.fail("Convertion timeout");
                }
            } catch (StaleElementReferenceException e) {
                flag = false;
            }
        }

    }

    @Step
    public void is_download_page_open() {
        getPages().currentPageAt(DownloadPage.class);
    }

    @Step
    public void check_locale_main_page_open(String path) {
        String checkingUrl;
        if (path.equals("/en/")) {
            checkingUrl = getPages().getDefaultBaseUrl() + "/";
        } else {
            checkingUrl = getPages().getDefaultBaseUrl() + path;
        }
        String currentUrl = getDriver().getCurrentUrl().replace("www.", "");
        assertThat(currentUrl, is(checkingUrl));
    }

    @Step
    public void get_current_url_before_click() {
        currentUrl = getDriver().getCurrentUrl();
    }

    @Step
    public void wait_for_page_change() {
        boolean isChanged = false;
        while (!isChanged) {
            isChanged = data.waitingForPageChanged(currentUrl);
        }

    }

    @Step
    public void wait_a_minute() {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {

            e.printStackTrace();
        }
    }

    @Step
    public void check_redirect() {
        ChromeWithProxy.proxy.stop();
        data.market_search(getPages().getDefaultBaseUrl());
    }

    @Step
    public void set_window_size(int width, int height) {
        getDriver().manage().window().setSize(new Dimension(width, height));
    }

    @Step
    public void download_converted_video_button_click() {
        try {
            downloadPage.downloadButtonClick(mobilePage);
        } catch (TimeoutException e) {

        }
    }

    @Step
    public void stop_convertion() {
        data.stopConvertion();
    }


}
