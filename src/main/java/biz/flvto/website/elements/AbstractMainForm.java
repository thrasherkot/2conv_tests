package biz.flvto.website.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by admin on 29.01.2017.
 */
public abstract class AbstractMainForm extends AbstractConversionForm{

    public AbstractMainForm(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    public void typeUrl(String url) {
        getUrlInput().sendKeys(url);
    }

    @Override
    public void startConversion() {
        getConvertButton().click();
    }

    protected abstract WebElement getUrlInput();

    protected abstract WebElement getConvertButton();

}
