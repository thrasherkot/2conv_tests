package biz.flvto.website.elements.interfaces;

/**
 * Created by admin on 28.01.2017.
 */
public interface RelatedVideos {

    public void selectConversionFormatForVideo(String format, int videoIndex);

    public void startConversion(int videoIndex);
}
