package biz.flvto.website.elements.interfaces;

/**
 * Created by admin on 28.01.2017.
 */
public interface ConversionForm {

    public void selectConversionFormat(String format);

    public void startConversion();
}
