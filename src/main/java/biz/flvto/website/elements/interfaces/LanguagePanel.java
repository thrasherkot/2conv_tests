package biz.flvto.website.elements.interfaces;

/**
 * Created by admin on 28.01.2017.
 */
public interface LanguagePanel {

    public void openLanguagePanel();

    public void chooseLanguage(String lang);
}
