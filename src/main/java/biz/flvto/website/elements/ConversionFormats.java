package biz.flvto.website.elements;

/**
 * Created by admin on 29.01.2017.
 */
public enum ConversionFormats {

    MP3("mp3","1"),
    MP4("mp4","8"),
    MP4_HD("mp4_hd","7"),
    AVI("avi","5"),
    AVI_HD("avi_hd","9")
    ;

    private String formatName;
    private String formatCode;

    ConversionFormats (String name, String code){
        this.formatName = name;
        this.formatCode = code;
    }

    public String getFormatName(){
        return formatName;
    }

    public String getFormatCode(){
        return formatCode;
    }

    public static ConversionFormats getFormatByName(String name){
        ConversionFormats formats [] = values();
        for (ConversionFormats format: formats){
            if (format.getFormatName().equals(name)){
                return format;
            }
        }
        throw new RuntimeException("There is no format with name " + name);
    }

}
