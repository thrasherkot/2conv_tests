package biz.flvto.website.elements;

import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by admin on 29.01.2017.
 */
public class RelatedVideo extends AbstractConversionForm {

    private String convertButtonLocator = "a.button.orange.convert";

    private String showAllFormatsButtonLocator = "div.button.orange";

    private String currenFormatLocator = "div.current-format-value";


    public RelatedVideo (WebDriver driver, WebElement webElement){
        super(driver, webElement);
    }

    @Override
    public void startConversion() {
        webElement.findElement(By.cssSelector(convertButtonLocator)).click();
    }

    @Override
    public void openConversionFormatList() {
        webElement.findElement(By.cssSelector(showAllFormatsButtonLocator)).click();
    }

    @Override
    public String getCurrentFormat() {
        return webElement.findElement(By.cssSelector(currenFormatLocator)).getText();
    }

}
