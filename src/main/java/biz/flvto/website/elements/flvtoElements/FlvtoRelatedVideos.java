package biz.flvto.website.elements.flvtoElements;

import biz.flvto.website.elements.AbstractRelatedVideosForm;
import biz.flvto.website.elements.RelatedVideo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by admin on 29.01.2017.
 */
public class FlvtoRelatedVideos extends AbstractRelatedVideosForm {

    public static String relatedFrameLocator = "iframe.related-frame";

    public FlvtoRelatedVideos(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    @Override
    protected List<WebElement> getRelatedFrameList() {
        return driver.findElements(By.cssSelector(relatedFrameLocator));
    }

    @Override
    public void convertRelatedVideo(int videoIndex, String format){
        checkRelatedFrameAndSwitchOnIt();
        RelatedVideo video = getRelatedVideos().get(videoIndex);
        video.selectConversionFormat(format);
        video.startConversion();
    }

}
