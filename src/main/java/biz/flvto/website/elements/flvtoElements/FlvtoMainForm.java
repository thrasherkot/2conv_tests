package biz.flvto.website.elements.flvtoElements;

import biz.flvto.website.elements.AbstractMainForm;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by admin on 28.01.2017.
 */
public class FlvtoMainForm extends AbstractMainForm {

    private String urlInputLocator = "convertUrl";

    private String convertButtonLocator = "//button[@type='submit']";

    private String showAllFormatButtonLocator = "showFormatsBtn";

    private String currentFormatFieldLocator = "current-format";

    public FlvtoMainForm(WebDriver driver, WebElement webElement){
        super(driver, webElement);
    }

    @Override
    protected WebElement getUrlInput(){
        return driver.findElement(By.id(urlInputLocator));
    }

    @Override
    protected WebElement getConvertButton(){
        return driver.findElement(By.xpath(convertButtonLocator));
    }

    @Override
    public void openConversionFormatList() {
        driver.findElement(By.id(showAllFormatButtonLocator));
    }

    @Override
    public String getCurrentFormat() {
        return driver.findElement(By.id(currentFormatFieldLocator)).getText();
    }
}
