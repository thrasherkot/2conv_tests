package biz.flvto.website.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 29.01.2017.
 */
public abstract class AbstractRelatedVideosForm extends AbstractPageElement{

    private String relatedVideoLocator = "div.related";

    public AbstractRelatedVideosForm(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    public List<RelatedVideo> getRelatedVideos(){
        List<RelatedVideo> videos = new ArrayList<>();
        List<WebElement> videosElements = driver.findElements(By.cssSelector(relatedVideoLocator));
        for (WebElement video:videosElements){
            videos.add(new RelatedVideo(driver, video));
        }
        return videos;
    }

    protected void checkRelatedFrameAndSwitchOnIt(){
        List<WebElement> relatedFrameList = getRelatedFrameList();
        if (relatedFrameList.size()>0){
            driver.switchTo().frame(relatedFrameList.get(0));
        }
    }

    protected abstract List<WebElement> getRelatedFrameList();

    public abstract void convertRelatedVideo(int videoIndex, String format);

}
