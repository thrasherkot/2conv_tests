package biz.flvto.website.elements;

import biz.flvto.website.elements.interfaces.ConversionForm;
import net.serenitybdd.core.annotations.findby.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * Created by admin on 29.01.2017.
 */
public abstract class AbstractConversionForm extends AbstractPageElement implements ConversionForm {

    private String formatOptionXpathPattern = "//*[@data-value='toReplace']";

    public AbstractConversionForm(WebDriver driver, WebElement webElement) {
        super(driver, webElement);
    }

    protected WebElement getFormatOption(String format){
        String replace = ConversionFormats.getFormatByName(format).getFormatCode();
        return driver.findElement(By.xpath(formatOptionXpathPattern.replace("toReplace",replace)));
    }

    @Override
    public void selectConversionFormat(String format){
        openConversionFormatList();
        getFormatOption(format).click();
    }

    public abstract void openConversionFormatList();

    public abstract String getCurrentFormat();

    public void checkCurrentFormatIs(String format) {
        String currentFormat = getCurrentFormat().toLowerCase();
        assertThat(currentFormat, is(format));
    }
}
