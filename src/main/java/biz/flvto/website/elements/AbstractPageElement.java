package biz.flvto.website.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by admin on 29.01.2017.
 */
public abstract class AbstractPageElement {

    protected WebDriver driver;

    protected WebElement webElement;

    public AbstractPageElement(WebDriver driver, WebElement webElement){
        this.driver = driver;
        this.webElement = webElement;
    }

}
