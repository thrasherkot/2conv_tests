package biz.flvto.website;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.lightbody.bmp.core.har.HarNameValuePair;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import biz.flvto.requirements.ParametersList;


public class DataWorker extends PageObject {

    public ArrayList<String> code = new ArrayList<>();
    private int counter = 0;
    List<String> ref = new ArrayList<>();

    public DataWorker(WebDriver driver) {
        setDriver(driver);
    }

    public void checkElementVisible(WebElement banner) {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(banner));
        element(banner).shouldBeCurrentlyVisible();
    }

    public void checkBanner(String zone) {
        String xpath = "//div[@data-zone='" + zone + "']";
        WebElement banner = getDriver().findElement(By.xpath(xpath));
        try {
            element(banner).isDisplayed();
        } catch (ElementNotVisibleException|StaleElementReferenceException ignored) {
            //this exceptions means that element exist but invisible
        } catch (ElementNotFoundException e) {
            Assert.fail("There is no banner with zone " + zone);
        }
    }

    public boolean waitingForPageChanged(String startUrl) {
        WebDriver driver = getDriver();
        boolean check = false;
        try {
            check = (new WebDriverWait(driver, 30)).until(new ExpectedCondition<Boolean>() {

                @Override
                public Boolean apply(WebDriver driver) {
                    String newUrl = driver.getCurrentUrl();
                    return !newUrl.equals(startUrl);
                }
            });
        } catch (TimeoutException e) {

        }
        return check;
    }

    public void switchToNewWindow(Set<String> oldWindowsSet) {
        WebDriver driver = getDriver();
        String newWindowHandle = (new WebDriverWait(driver, 10))
                .until(new ExpectedCondition<String>() {
                           public String apply(WebDriver driver) {
                               Set<String> newWindowsSet = driver.getWindowHandles();
                               newWindowsSet.removeAll(oldWindowsSet);
                               return newWindowsSet.size() > 0 ?
                                       newWindowsSet.iterator().next() : null;
                           }
                       }
                );
        driver.switchTo().window(newWindowHandle);
    }

    public String generateEmail() {
        Random r = new Random();
        return "WebDriverMail" + r.nextInt(1000) + "@gmail.com";
    }

    public void acceptAllert() {
        WebDriver driver = getDriver();
        new WebDriverWait(driver, 60)
                .ignoring(NoAlertPresentException.class)
                .until(ExpectedConditions.alertIsPresent());

        Alert al = driver.switchTo().alert();
        al.accept();
    }

    public void waitForElement(WebElement element) {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(element));
    }

    public void robotsChecker(String path, String currentRobots) {
        FileInputStream inFile;
        try {
            inFile = new FileInputStream(System.getProperty("user.dir") + path);
            byte[] str = new byte[inFile.available()];
            inFile.read(str);
            String text = new String(str);
            text = text.replaceAll("\r", "");
            assertThat(currentRobots, is(text));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void stopConvertion() {
        try {
            ((JavascriptExecutor) getDriver()).executeScript
                    ("javascript:(function () {stopped = $('body').hasClass('stopped');"
                            + "if (stopped) {Track.start();$('body').removeClass('stopped')} "
                            + "else {Track.stop();$('body').addClass('stopped')}})();");
        } catch (WebDriverException e) {

        }
    }

    public void checkDownloadFileResponseFromApi() {
        Har har = ChromeWithProxy.proxy.getHar();
        List<HarEntry> entries = har.getLog().getEntries();
        boolean status = false;
        for (HarEntry entry : entries) {
            Assert.assertNotNull("entry is null", entry);
            if (entry.getServerIPAddress().equals(ParametersList.localConvertIp)) {
                status = true;
                int respCode = entry.getResponse().getStatus();
                if (respCode != 200) {
                    Assert.fail("download status wrong. " + entry.getRequest().getUrl() + " " + respCode);
                }
                break;
            }
        }
        if (!status) {
            Assert.fail("There is no download request");
        }
    }

    public void savePageCode(Har har) {
        List<HarEntry> entries = har.getLog().getEntries();
        for (HarEntry entry : entries) {
            Assert.assertNotNull("entry is null", entry);
            if (entry.getRequest().getUrl().contains("banner_stats")) {
                for (HarNameValuePair part : entry.getRequest().getHeaders()) {
                    if (part.getName().equals("Referer")) {
                        code.add(entry.getRequest().getUrl() + " " + part.getValue());
                        break;
                    }
                }

            }
        }

        writeCode();
    }

    private void writeCode() {
        FileWriter outFile;
        try {
            outFile = new FileWriter(System.getProperty("user.dir") + "\\target\\code.txt");
            PrintWriter out = new PrintWriter(outFile);
            for (String rec : code) {
                out.print(rec + "\n");
            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void market_search(String site) {
        Har har = ChromeWithProxy.proxy.getHar();
        List<HarEntry> entries = har.getLog().getEntries();
        String cdn = site.replace("http://", "cdn");
        List<HarEntry> reverseHarList = new ArrayList<>();
        for (HarEntry entry : entries) {
            Assert.assertNotNull("entry is null", entry);
            reverseHarList.add(entry);
            String url = entry.getRequest().getUrl();
            if (url.contains("itunes") || url.contains("play.google.com")) {
                savePageCode(har);
                String badBanner = refererParser(reverseHarList, entry, cdn);
                Assert.fail("redirect to " + entry.getRequest().getUrl() + " from " +
                        badBanner + " " + code.toString() + " " + counter + " " + ref.toString());
            }
        }
    }

    private String refererParser(List<HarEntry> entries, HarEntry entry, String cdn) {
        counter++;
        String result = "";
        String referer = "";
        List<HarNameValuePair> header = entry.getRequest().getHeaders();
        for (HarNameValuePair part : header) {
            if (part.getName().equals("Referer")) {
                referer = part.getValue();
                ref.add(referer);
                break;
            }
        }
        if (referer.contains(cdn)) result = referer;
        else {
            for (int i = entries.size() - 1; i >= 0; i--) {
                HarEntry newEntry = entries.get(i);
                if (newEntry.getRequest().getUrl().equals(referer)) {
                    result = refererParser(entries, newEntry, cdn);
                    break;
                }
            }
            if (result.equals("")) {
                result = referer;
            }
        }
        return result;
    }


}
