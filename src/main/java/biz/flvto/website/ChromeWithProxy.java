package biz.flvto.website;

import java.net.InetSocketAddress;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import net.lightbody.bmp.proxy.auth.AuthType;
import net.thucydides.core.webdriver.DriverSource;
import biz.flvto.requirements.ParametersList;

public class ChromeWithProxy implements DriverSource {

    public static BrowserMobProxy proxy;

    @Override
    public WebDriver newDriver() {
        proxy = new BrowserMobProxyServer();
        ParametersList.getProxy();
        if (!ParametersList.proxyHost.equals("")) {
            proxy.setChainedProxy(new InetSocketAddress(ParametersList.proxyHost, ParametersList.proxyPort));
            proxy.chainedProxyAuthorization(ParametersList.proxyUser, ParametersList.proxyPass, AuthType.BASIC);
        }
        proxy.setTrustAllServers(true);
        proxy.setHarCaptureTypes(CaptureType.REQUEST_HEADERS);
        proxy.start(0);
        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.PROXY, seleniumProxy);
        if (!ParametersList.userAgent.equals("")) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--user-agent=" + ParametersList.userAgent);

            cap.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "normal");
            cap.setCapability(ChromeOptions.CAPABILITY, options);

            proxy.newHar();
        }

        return new ChromeDriver(cap);
    }

    @Override
    public boolean takesScreenshots() {
        return true;
    }


}
