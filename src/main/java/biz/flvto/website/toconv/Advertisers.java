package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/advertisers/"})
public class Advertisers extends HeaderPlusFooter {

    public Advertisers(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "advertisers_form_name")
    public WebElement advertiserNameInput;

    @FindBy(id = "advertisers_form_email")
    public WebElement advertiserEmailInput;

    @FindBy(id = "advertisers_form_site")
    public WebElement advertiserWebsiteInput;

    @FindBy(id = "advertisers_form_skype")
    public WebElement advertiserSkypeInput;

    @FindBy(id = "advertisers_form_usCpm")
    public WebElement usCPMInput;

    @FindBy(id = "advertisers_form_geo")
    public WebElement geolocationInput;

    @FindBy(id = "advertisers_form_cpm")
    public WebElement cPMOfferInput;

    @FindBy(id = "advertisers_form_message")
    public WebElement messageTextarea;

    @FindBy(id = "advertisers_form_captcha")
    public WebElement captchaInput;

    @FindBy(id = "advertisers_form_send")
    public WebElement sendButton;


    public void isElementsVisible() {
        data.checkElementVisible(advertiserNameInput);
        data.checkElementVisible(advertiserEmailInput);
        data.checkElementVisible(advertiserWebsiteInput);
        data.checkElementVisible(advertiserSkypeInput);
        data.checkElementVisible(usCPMInput);
        data.checkElementVisible(geolocationInput);
        data.checkElementVisible(cPMOfferInput);
        data.checkElementVisible(messageTextarea);
        data.checkElementVisible(captchaInput);
        data.checkElementVisible(sendButton);
    }

}
