package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/youtube-downloader/",
        "#HOST/*.*/youtube-downloader-for-mac/*.*"})
public class DesktopConverter extends HeaderPlusFooter {

    public DesktopConverter(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@href,'software-download')]")
    public WebElement downloadButton;


    public void checkDownloadButtonVisible() {
        data.checkElementVisible(downloadButton);
    }

}
