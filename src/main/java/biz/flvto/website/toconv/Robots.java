package biz.flvto.website.toconv;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.At;
import biz.flvto.website.DataWorker;

@At(urls = {"#HOST/robots.txt"})
public class Robots extends PageObject {

    @FindBy(css = "pre")
    public WebElement robots;

    private DataWorker data;

    public void checkRobots() {
        String currentRobots = robots.getText();
        String path = "\\src\\website\\toconv\\robots.txt";
        data.robotsChecker(path, currentRobots);


    }

}
