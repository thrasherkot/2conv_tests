package biz.flvto.website.toconv;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;


@At(urls = {"#HOST/*.*/",
        "#HOST/*.*/youtube-mp3/",
        "#HOST/*.*/youtube-avi/",
        "#HOST/*.*/youtube-mp4/",
        "#HOST/*.*/youtube-video-downloader/"})
public class MainPage extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"5", "10", "11", "806", "807", "815"};
    private static final String mobileDataZones[] = {"112", "74"};

    public MainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "convertUrl")
    public WebElement youtubeLinkInput;

    @FindBy(xpath = "//button[contains(@onclick,'Convert')]")
    public WebElement convertButton;

    @FindBy(id = "showFormatsBtn")
    public WebElement showFormatButton;

    @FindBy(id = "current-format")
    public WebElement currentFormatField;

    @FindBy(xpath = "//li[@data-value='1']")
    public WebElement mp3SelectButton;

    @FindBy(xpath = "//li[@data-value='8']")
    public WebElement mp4SelectButton;

    @FindBy(xpath = "//li[@data-value='7']")
    public WebElement mp4HDSelectButton;

    @FindBy(xpath = "//li[@data-value='5']")
    public WebElement aviSelectButton;

    @FindBy(xpath = "//li[@data-value='9']")
    public WebElement aviHDSelectButton;

    @FindBy(xpath = "//a[contains(@class,'converter')]")
    public WebElement downloadConverterButton;

    @FindBy(xpath = "//iframe[@class='related']")
    public WebElement relatedFrame;

    @FindBy(xpath = "//div[@class='popup']")
    public WebElement errorPopup;

    @FindBy(xpath = "//a[contains(@href, 'source=9')]")
    public WebElement popupDownloadButton;

    @FindBy(xpath = "//a[contains(@href, 'bug_report')]")
    public WebElement popupSendFeedbackLink;

    //mirrors

    @FindBy(xpath = "//a[contains(@href, '/youtube-mp3/')]")
    public WebElement youtubeToMP3Link;

    @FindBy(xpath = "//a[contains(@href, '/youtube-avi/')]")
    public WebElement youtubeToAviLink;

    @FindBy(xpath = "//a[contains(@href, '/youtube-mp4/')]")
    public WebElement youtubeToMP4Link;

    @FindBy(xpath = "//a[contains(@href, '/youtube-video-downloader/')]")
    public WebElement youtubeVideoDownloaderLink;


    public void typeUrl(String link) {
        typeInto(youtubeLinkInput, link);
    }

    public void convertButtonClick() {
        convertButton.click();
    }

    public void showFormatButtonClick() {
        showFormatButton.click();
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(aviHDSelectButton));
    }

    public void mp3SelectButtonClick() {
        mp3SelectButton.click();
    }

    public void mp4SelectButtonClick() {
        mp4SelectButton.click();
    }

    public void mp4HDSelectButtonClick() {
        mp4HDSelectButton.click();
    }

    public void aviSelectButtonClick() {
        aviSelectButton.click();
    }

    public void aviHDSelectButtonClick() {
        aviHDSelectButton.click();
    }

    public void downloadConverterButtonClick() {
        downloadConverterButton.click();
    }

    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkCurrentFormatIs(String format) {
        String currentFormat = currentFormatField.getText().toLowerCase();
        assertThat(currentFormat, is(format));
    }

    public void swithToRelatedFrame() {
        getDriver().switchTo().frame(relatedFrame);
    }

    public void checkErrorPopupVisible() {
        data.checkElementVisible(errorPopup);
    }

    public void popupDownloadButtonClick() {
        popupDownloadButton.click();
    }

    public void popupSendFeedbackLinkClick() {
        popupSendFeedbackLink.click();
    }

    public void youtubeVideoDownloaderLinkClick() {
        youtubeVideoDownloaderLink.click();
    }

    public void youtubeToAviLinkClick() {
        youtubeToAviLink.click();
    }

    public void youtubeToMP3LinkClick() {
        youtubeToMP3Link.click();
    }

    public void youtubeToMP4LinkClick() {
        youtubeToMP4Link.click();
    }


}
