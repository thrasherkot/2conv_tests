package biz.flvto.website.toconv;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class Related extends PageObject {


    @FindBy(xpath = "//a[@class='button convert']")
    public WebElement convertRelatedButton;

    @FindBy(xpath = "//div[@class='current-format-value']")
    public WebElement currentFormatField;

    @FindBy(xpath = "//div[@class='button current-format']")
    public WebElement showFormatButton;

    @FindBy(xpath = "//li[@data-value='1']")
    public WebElement mp3SelectButton;

    @FindBy(xpath = "//li[@data-value='8']")
    public WebElement mp4SelectButton;

    @FindBy(xpath = "//li[@data-value='7']")
    public WebElement mp4HDSelectButton;

    @FindBy(xpath = "//li[@data-value='5']")
    public WebElement aviSelectButton;

    @FindBy(xpath = "//li[@data-value='9']")
    public WebElement aviHDSelectButton;


    public void convertButtonClick() {
        convertRelatedButton.click();
    }

    public void showFormatButtonClick() {
        showFormatButton.click();
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(aviHDSelectButton));
    }

    public void mp3SelectButtonClick() {
        mp3SelectButton.click();
    }

    public void mp4SelectButtonClick() {
        mp4SelectButton.click();
    }

    public void mp4HDSelectButtonClick() {
        mp4HDSelectButton.click();
    }

    public void aviSelectButtonClick() {
        aviSelectButton.click();
    }

    public void aviHDSelectButtonClick() {
        aviHDSelectButton.click();
    }

    public void checkCurrentFormatIs(String format) {
        String currentFormat = currentFormatField.getText().toLowerCase();
        assertThat(currentFormat, is(format));
    }


}
