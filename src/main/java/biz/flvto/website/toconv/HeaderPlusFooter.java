package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import biz.flvto.website.DataWorker;

public abstract class HeaderPlusFooter extends PageObject {

    protected DataWorker data;

    public HeaderPlusFooter(WebDriver driver) {
        super(driver);
        this.data = new DataWorker(driver);
    }

    //header

    @FindBy(xpath = "//a[@class='logo']")
    public WebElement headerMainPageLink;

    @FindBy(xpath = "//nav[@class='bg-gray-800']//li[1]/a")
    public WebElement headerHomeLink;

    @FindBy(xpath = "//a[contains(@onclick, 'Header') and contains(@href, 'downloader')]")
    public WebElement headerDownloadConverterLink;

    @FindBy(xpath = "//nav[@class = 'bg-gray-800']//a[contains(@href, 'feedback')]")
    public WebElement headerSupportLink;

    @FindBy(xpath = "//a[@class = 'language-switcher']")
    public WebElement headerLanguageSwitcher;

    @FindBy(css = "div.icon.menu")
    public WebElement mobileMenu;


    //footer

    @FindBy(xpath = "//footer//a[contains(@href, 'feedback') and not(contains(@href,'report'))]")
    public WebElement footerFeedbackLink;

    @FindBy(xpath = "//a[contains(@href, 'advertisers')]")
    public WebElement footerAdvertisersLink;

    @FindBy(xpath = "//a[contains(@href,'terms')]")
    public WebElement footerTermsLink;

    @FindBy(xpath = "//a[contains(@href,'policy')]")
    public WebElement footerPolicyLink;

    @FindBy(xpath = "//a[contains(@href,'faq')]")
    public WebElement footerFaqLink;

    @FindBy(xpath = "//footer//a[contains(@href,'report')]")
    public WebElement footerReportAbuseLink;

    @FindBy(xpath = "//div[@class = 'logo']/a")
    public WebElement footerMainPageLink;


    //mirors

    @FindBy(xpath = "//div[contains(@class, 'mirrors')]//a[contains(@href,'downloader')]")
    public WebElement mirrorYouTubeDownloaderLink;

    @FindBy(xpath = "//div[contains(@class, 'mirrors')]//a[contains(@href,'avi')]")
    public WebElement mirrorYouTubeToAviLink;

    @FindBy(xpath = "//div[contains(@class, 'mirrors')]//a[contains(@href,'mp3')]")
    public WebElement mirrorYouTubeToMP3Link;

    @FindBy(xpath = "//div[contains(@class, 'mirrors')]//a[contains(@href,'mp4')]")
    public WebElement mirrorYouTubeToMP4Link;


    //social

    @FindBy(xpath = "//a[contains(@href,'twitter')]")
    public WebElement footerTwitterLink;

    @FindBy(xpath = "//a[contains(@href,'facebook')]")
    public WebElement footerFacebookLink;

    @FindBy(xpath = "//a[contains(@href,'google')]")
    public WebElement footerGooglekLink;


    public void headerMainPageLinkClick() {
        headerMainPageLink.click();
    }

    public void headerHomeLinkClick() {
        headerHomeLink.click();
    }

    public void headerDownloadConverterLinkClick() {
        headerDownloadConverterLink.click();
    }

    public void languageSwitcherClick() {
        headerLanguageSwitcher.click();
    }

    public void headerSupportLinkClick() {
        headerSupportLink.click();
    }

    public void footerFeedbackLinkClick() {
        footerFeedbackLink.click();
    }

    public void footerAdvertisersLinkCLick() {
        footerAdvertisersLink.click();
    }

    public void footerTermsLinkClick() {
        footerTermsLink.click();
    }

    public void footerPolicyLinkClick() {
        footerPolicyLink.click();
    }

    public void footerFaqLinkCLick() {
        footerFaqLink.click();
    }

    public void footerReportAbuseLinkClick() {
        footerReportAbuseLink.click();
    }

    public void footerMainPageLinkCLick() {
        footerMainPageLink.click();
    }

    public void waitForPageChanging(String startUrl) {
        data.waitingForPageChanged(startUrl);
    }

    public void selectLanguage(String lang) {
        String locator = "span.language-icon." + lang;
        WebElement link = getDriver().findElement(By.cssSelector(locator));
        data.waitForElement(link);
        link.click();
    }

    public void mobileMenuClick() {
        mobileMenu.click();
    }

}
