package biz.flvto.website.toconv;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/terms/",
        "#HOST/*.*/policy/",
        "#HOST/*.*/faq/"})
public class InfoPages extends HeaderPlusFooter {

    public InfoPages(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".h2")
    public WebElement mainHeader;

    @FindBy(css = ".h4")
    public List<WebElement> subHeaders;


    public void checkHeadersCounter(int counter) {
        assertThat(subHeaders.size(), is(counter));
    }

    public void isMainLabelVisible() {
        data.checkElementVisible(mainHeader);
    }


}
