package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/download/*.*"})
public class Download extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"5", "10", "11", "806", "787", "815"};
    private static final String mobileDataZones[] = {"112", "74"};

    public Download(WebDriver driver) {
        super(driver);
    }


    @FindBy(xpath = "//a[contains(@onclick,'Download')]")
    public WebElement downloadButton;

    @FindBy(xpath = "//a[contains(@onclick,'downloader')]")
    public WebElement downloadLandingButton;

    @FindBy(xpath = "//a[contains(@onclick,'another')]")
    public WebElement convertAnotherVideoButton;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void downloadLandingButtonClick() {
        downloadLandingButton.click();
    }

    public void convertAnotherVideoButtonClick() {
        convertAnotherVideoButton.click();
    }


    public void waitForDonwloadButtonVisible() {
        data.waitForElement(downloadButton);
    }

    public void downloadButtonClick() {
        downloadButton.click();
    }
}
