package biz.flvto.website.toconv;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/progress/*.*"})
public class Progress extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"5", "10", "11", "806", "815"};
    private static final String mobileDataZones[] = {"112", "74", "166"};

    public Progress(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='percent']")
    public WebElement progressValueField;

    @FindBy(xpath = "//a[contains(@onclick,'Convert')]")
    public WebElement convertAnotherVideoButton;

    @FindBy(xpath = "//div[@class = 'popup']")
    public WebElement convertErrorForm;

    @FindBy(xpath = "//a[contains(@onclick,'Error')]")
    public WebElement popupDownloadButton;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }


    public int getProgressValue() {
        String result = "101";
        try {
            result = progressValueField.getText();
        } catch (NoSuchElementException e) {

        } catch (StaleElementReferenceException e) {

        }
        return Integer.valueOf(result);
    }

    public boolean isErrorFormVisible() {
        return element(convertErrorForm).isVisible();
    }

    public void popupDownloadButtonClick() {
        popupDownloadButton.click();
    }

    public void waitForPopupError() {
        data.checkElementVisible(convertErrorForm);
    }

    public void waitForPorgressPage() {
        new WebDriverWait(getDriver(), 7).until(ExpectedConditions.elementToBeClickable(progressValueField));
    }


}
