package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/feedback/*.*"})
public class Support extends HeaderPlusFooter {

    public Support(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "feedback_form_email")
    public WebElement emailInput;

    @FindBy(id = "feedback_form_sourceUrl")
    public WebElement sourceUrlInput;

    @FindBy(id = "feedback_form_message")
    public WebElement feedbackMesageTextarea;

    @FindBy(id = "feedback_form_captcha")
    public WebElement captchaInput;

    @FindBy(id = "feedback_form_send")
    public WebElement sendFeedbackButton;


    public void isElementsVisible() {
        data.checkElementVisible(emailInput);
        data.checkElementVisible(sourceUrlInput);
        data.checkElementVisible(feedbackMesageTextarea);
        data.checkElementVisible(captchaInput);
        data.checkElementVisible(sendFeedbackButton);
    }


}
