package biz.flvto.website.toconv;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;

public class Page404 extends HeaderPlusFooter {

    public Page404(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@onclick,'downloader')]")
    public WebElement downloadLandingButton;


}
