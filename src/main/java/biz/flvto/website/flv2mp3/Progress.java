package biz.flvto.website.flv2mp3;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/progress/*.*"})
public class Progress extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"814", "14", "33", "15", "16"};
    private static final String mobileDataZones[] = {"85", "167"};

    public Progress(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "percent")
    public WebElement progressValueField;

    @FindBy(id = "convert-error")
    public WebElement convertErrorForm;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public int getProgressValue() {
        String result = "101";
        try {
            result = progressValueField.getText();
        } catch (NoSuchElementException e) {

        } catch (StaleElementReferenceException e) {

        }
        return Integer.valueOf(result);
    }

    public boolean isErrorFormVisible() {
        return element(convertErrorForm).isVisible();
    }

    public void waitForPopupError() {
        data.checkElementVisible(convertErrorForm);
    }

    public void waitForPorgressPage() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(progressValueField));
    }


}
