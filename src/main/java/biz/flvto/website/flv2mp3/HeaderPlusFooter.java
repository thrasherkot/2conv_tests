package biz.flvto.website.flv2mp3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import biz.flvto.website.DataWorker;

public class HeaderPlusFooter extends PageObject {

    protected DataWorker data;

    public HeaderPlusFooter(WebDriver driver) {
        super(driver);
        this.data = new DataWorker(driver);
    }

    @FindBy(xpath = "//div[@class='current-lang']")
    public WebElement languageSelector;

    @FindBy(xpath = "//a[@title='Flv2mp3.org']")
    public WebElement mainPageLink;


    @FindBy(xpath = "//a[@href='/terms/']")
    public WebElement termsFooterLink;

    @FindBy(xpath = "//a[@href='/policy/']")
    public WebElement policyFooterLink;

    @FindBy(xpath = "//a[@href='/faq/']")
    public WebElement faqFooterLink;

    @FindBy(xpath = "//a[@href='/feedback/']")
    public WebElement feedbackFooterLink;

    @FindBy(xpath = "//a[@href='/advertisers/']")
    public WebElement advertisersFooterLink;

    //social elements

    @FindBy(xpath = "//div[@class='fb-like fb_iframe_widget']")
    public WebElement facebookLike;

    @FindBy(id = "___plusone_0")
    public WebElement googlePlusLike;

    @FindBy(id = "twitter-widget-0")
    public WebElement twitterTweet;


    public void mainPageLinkClick() {
        mainPageLink.click();
    }


}
