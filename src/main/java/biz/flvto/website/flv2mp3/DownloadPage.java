package biz.flvto.website.flv2mp3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/download/*.*"})
public class DownloadPage extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"814", "14", "187", "33", "15", "16"};
    private static final String mobileDataZones[] = {"85"};

    public DownloadPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(css = "a.download-button")
    public WebElement downloadButton;

    @FindBy(xpath = "//a[contains(@href,'direct')]")
    public WebElement mobileDownloadButton;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void waitForDownloadButtonVisible(boolean mobilePage) {
        if (mobilePage) {
            data.waitForElement(mobileDownloadButton);
        } else {
            data.waitForElement(downloadButton);
        }
    }

    public void downloadButtonClick(boolean mobilePage) {
        if (mobilePage) {
            mobileDownloadButton.click();
        } else {
            downloadButton.click();
        }
    }

}
