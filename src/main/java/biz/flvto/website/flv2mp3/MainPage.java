package biz.flvto.website.flv2mp3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/"})
public class MainPage extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"814", "14", "33", "15", "16"};
    private static final String mobileDataZones[] = {"85"};

    public MainPage(WebDriver driver) {
        super(driver);
    }


    @FindBy(id = "convertUrl")
    public WebElement linkInput;

    @FindBy(xpath = "//button[@type='submit']")
    public WebElement convertButton;

    @FindBy(id = "convert_convert")
    public WebElement mobileConvertButton;

    @FindBy(xpath = "//li[text()='MP3']/span[@class='radio']")
    public WebElement mp3RadioSelector;

    @FindBy(xpath = "//li[text()='MP4']/span[@class='radio']")
    public WebElement mp4RadioSelector;

    @FindBy(xpath = "//li[contains(text(),'MP4 ')]/span[@class='radio']")
    public WebElement mp4HDRadioSelector;

    @FindBy(xpath = "//li[text()='AVI']/span[@class='radio']")
    public WebElement aviRadioSelector;

    @FindBy(xpath = "//li[contains(text(),'AVI ')]/span[@class='radio']")
    public WebElement aviHDRadioSelector;

    @FindBy(xpath = "//span[text()='Download converter for free']")
    public WebElement downloadConverterButton;

    @FindBy(id = "span-format")
    public WebElement formatOnButton;


    @FindBy(xpath = "//div[text()='Music Blog']")
    public WebElement musicBlogLink;

    @FindBy(xpath = "//a[@rel='nofollow']")
    public WebElement downloadAppLink;

    @FindBy(xpath = "//a[contains(text(),'learn more')]")
    public WebElement learnMoreLink;

    @FindBy(id = "facebook")
    public WebElement facebookDocument;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void typeYoutubeLink(String link) {
        typeInto(linkInput, link);
    }

    public void convertButtonClick() {
        convertButton.click();
    }

    public void mobileConvertButtonClick() {
        mobileConvertButton.click();
    }

    public void selectMP3Format() {
        mp3RadioSelector.click();
    }

    public void selectMP4Format() {
        mp4RadioSelector.click();
    }

    public void selectMP4HDFormat() {
        mp4HDRadioSelector.click();
    }

    public void selectAviFormat() {
        aviRadioSelector.click();
    }

    public void selectAviHDFormat() {
        aviHDRadioSelector.click();
    }

    public void downloadConverterButtonClick() {
        downloadConverterButton.click();
    }


}
