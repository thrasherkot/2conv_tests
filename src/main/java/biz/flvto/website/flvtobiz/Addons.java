package biz.flvto.website.flvtobiz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/addon/"})
public class Addons extends HeaderPlusFooter {

    public Addons(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='back-to-home']/span")
    public WebElement homeLink;

    @FindBy(xpath = "//span[contains(@onclick,'FireFox')]")
    public WebElement firefoxPluginLink;

    @FindBy(xpath = "//span[contains(@onclick,'Chrome')]")
    public WebElement chromePluginLink;

    @FindBy(xpath = "//span[contains(@onclick,'IE')]")
    public WebElement iePluginLink;

    @FindBy(xpath = "//span[contains(@onclick,'Safari')]")
    public WebElement safariPluginLink;


    public void checkFirefoxLinkAvailible() {
        data.checkElementVisible(firefoxPluginLink);
    }

    public void checkChromeLinkAvailible() {
        data.checkElementVisible(chromePluginLink);
    }

    public void checkIeLinkAvailible() {
        data.checkElementVisible(iePluginLink);
    }

    public void checkSafariLinkAvailible() {
        data.checkElementVisible(safariPluginLink);
    }

}
