package biz.flvto.website.flvtobiz;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/terms/"})
public class TermsOfService extends HeaderPlusFooter {

    public TermsOfService(WebDriver driver) {
        super(driver);
    }

    String headersXpath = "//h1";
    public List<WebElement> h2List = new ArrayList<>();

    public void checkHeadersCounter(int counter) {
        h2List = getDriver().findElements(By.xpath(headersXpath));
        assertThat(h2List.size(), is(counter));
    }


}
