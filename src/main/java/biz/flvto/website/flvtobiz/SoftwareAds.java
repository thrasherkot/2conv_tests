package biz.flvto.website.flvtobiz;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.lightbody.bmp.core.har.Har;
import net.lightbody.bmp.core.har.HarEntry;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.At;
import biz.flvto.website.ChromeWithProxy;
import biz.flvto.website.DataWorker;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@At(urls = {"#HOST/software_ads/"})
public class SoftwareAds extends PageObject {

    private static final String dataZone = "835";

    private DataWorker data;


    public void checkBanner() {
        data.checkBanner(dataZone);
    }

    public void checkPageLoadStatus() {
        Har har = ChromeWithProxy.proxy.getHar();
        List<HarEntry> entries = har.getLog().getEntries();
        boolean status = false;
        for (HarEntry entry : entries) {
            Assert.assertNotNull("entry is null", entry);
            if (entry.getRequest().getUrl().contains(getDriver().getCurrentUrl())) {
                status = true;
                int respCode = entry.getResponse().getStatus();
                if (respCode != 200) {
                    Assert.fail("Page wasn't open. Status =  " + respCode + " " + entry.getResponse().getRedirectURL()
                            + " " + entry.getRequest().getUrl());
                }
                break;
            }
        }
        if (!status) {
            Assert.fail("There is no page request detected");
        }

    }

    public void checkOnlyOneBannerZoneHere() {        //��������, ��� ��������� ���� �� �������� ������� ����
        List<WebElement> banners = getDriver().findElements(By.xpath("//div[@data-zone]"));
        assertThat(banners.size(), is(1));
    }

}
