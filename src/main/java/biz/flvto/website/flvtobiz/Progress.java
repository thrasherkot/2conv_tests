package biz.flvto.website.flvtobiz;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;


@At(urls = {"#HOST/*.*/progress/*.*"})
public class Progress extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"812", "18", "9", "6", "150"};
    private static final String mobileDataZones[] = {"52"};

    public Progress(WebDriver driver) {
        super(driver);
    }


    @FindBy(id = "percent")
    public WebElement progressValueField;

    @FindBy(css = "div.popup")
    public WebElement convertErrorForm;

    @FindBy(xpath = "//a[@class='link-abort']")
    public WebElement cancelConvertationButton;

    @FindBy(xpath = "//a[@class='button']")
    public WebElement popupDownloadButton;

    @FindBy(xpath = "//a[contains(@onclick,'another')]")
    public WebElement convertAnotherButton;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public int getProgressValue() {
        String result = "101";
        try {
            result = progressValueField.getText();
        } catch (NoSuchElementException e) {

        } catch (StaleElementReferenceException e) {

        }
        return Integer.valueOf(result);
    }

    public boolean isErrorFormVisible() {
        return element(convertErrorForm).isVisible();
    }

    public void cancelConvertationButtonClick() {
        cancelConvertationButton.click();
    }

    public void popupDownloadButtonClick() {
        popupDownloadButton.click();
    }

    public void waitForPopupError() {
        data.checkElementVisible(convertErrorForm);
    }

    public void waitForPorgressPage() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(convertAnotherButton));
    }
}
