package biz.flvto.website.flvtobiz;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/download/*.*"})
public class DownloadPage extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"812", "18", "9", "6", "150", "178", "168"};
    private static final String mobileDataZones[] = {"186", "52"};

    public DownloadPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(@href,'direct')]")
    public WebElement downloadButton;

    @FindBy(id = "btn-download-using")
    public WebElement downloadUsingAppButton;

    @FindBy(xpath = "//a[text()='Download converter for free']")
    public WebElement downloadPageTabLink;

    @FindBy(xpath = "//a[contains(@onclick,'Email')]")
    public WebElement sendToEmailLink;

    @FindBy(xpath = "//a[@class='dropbox-container show-tooltip']/span")
    public WebElement saveToDropboxLink;

    @FindBy(xpath = "//a[@class='ringtone show-tooltip']/span")
    public WebElement saveToRingtoneLink;

    @FindBy(name = "email")
    public WebElement emailInput;

    @FindBy(xpath = "//button[contains(@class,'submit')]")
    public WebElement sendButton;

    @FindBy(xpath = "//div[@class='popup']/div[@class='title']")
    public WebElement mailSendPopup;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void downloadButtonClick() {
        downloadButton.click();
    }

    public void downloadUsingAppButtonClick() {
        try {
            downloadUsingAppButton.click();
        } catch (TimeoutException e) {

        }
    }

    public void dowloadPageTabLinkClick() {
        downloadPageTabLink.click();
    }

    public void sendToEmailLinkClick() {
        sendToEmailLink.click();
    }

    public void typeEmail(String email) {
        data.checkElementVisible(emailInput);
        typeInto(emailInput, email);
    }

    public void sendToEmailButtonClick() {
        sendButton.click();
    }

    public void saveToDropboxLinkClick() {
        saveToDropboxLink.click();
    }

    public void saveToRingtoneLinkClick() {
        saveToRingtoneLink.click();
    }

    public void checkEmailSuccessfullSended() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(mailSendPopup));
        assertThat(mailSendPopup.getText(), containsString("Thank you"));
    }


    public void waitForDonwloadButtonVisible() {
        data.waitForElement(downloadButton);
    }

}
