package biz.flvto.website.flvtobiz;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class Page404 extends PageObject {

    @FindBy(id = "download-d")
    public WebElement downloadButton;

    public void downloadButtonClick() {
        downloadButton.click();
    }

    public void checkDownloadButtonVisible() {
        element(downloadButton).waitUntilEnabled();
    }

}
