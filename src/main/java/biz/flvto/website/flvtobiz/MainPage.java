package biz.flvto.website.flvtobiz;

import biz.flvto.website.elements.flvtoElements.FlvtoMainForm;
import biz.flvto.website.elements.interfaces.ConversionForm;
import org.openqa.selenium.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@At(urls = {"#HOST/*.*/",
        "#HOST/*.*/youtube-to-mp3/",
        "#HOST/*.*/youtube-to-avi/",
        "#HOST/*.*/youtube-to-avi-hd/",
        "#HOST/*.*/youtube-to-mp4/",
        "#HOST/*.*/youtube-to-mp4-hd/"})
public class MainPage extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"812", "18", "9", "6", "150", "168"};
    private static final String mobileDataZones[] = {"819"};


    public MainPage(WebDriver driver) {
        super(driver);
    }

    public static String conversionFormLocator = "convertForm";

    @FindBy(xpath = "//a[@class='button redirect-to-manual']")
    public WebElement downloadAppButton;

    @FindBy(xpath = "//div[contains(@class,'tips')]/a[contains(@href,'?source=1')]")
    public WebElement downloadSoftwareTipsButton;

    @FindBy(xpath = "//iframe[contains(@src,'/related/')]")
    public WebElement relatedFrame;

    @FindBy(id = "convert-modal")
    public WebElement errorPopup;

    @FindBy(xpath = "//a[@class='button']")
    public WebElement popupDownloadButton;


    @FindBy(xpath = "//div[@class='fb-like-box fb_iframe_widget']")
    public WebElement facebookLikeBox;

    @FindBy(xpath = "//a[@href='/youtube-to-mp3/']")
    public WebElement youtubeToMp3Link;

    @FindBy(xpath = "//a[@href='/youtube-to-avi/']")
    public WebElement youtubeToAviLink;

    @FindBy(xpath = "//a[@href='/youtube-to-avi-hd/']")
    public WebElement youtubeToAviHDLink;

    @FindBy(xpath = "//a[@href='/youtube-to-mp4/']")
    public WebElement youtubeToMP4Link;

    @FindBy(xpath = "//a[@href='/youtube-to-mp4-hd/']")
    public WebElement youtubeToMP4HDLink;

    @FindBy(css = "div.header-pic.howto_desktop-sprite.howto_desktop-header-pic")
    public WebElement mirrorImage;


    public void downloadAppButtonClick() {
        downloadAppButton.click();
    }

    public void downloadSoftwareTipsButtonClick() {
        downloadSoftwareTipsButton.click();
    }


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkFacebookBanner() {
        data.checkElementVisible(facebookLikeBox);
    }

    public void swithToRelatedFrame() {
        getDriver().switchTo().frame(relatedFrame);
    }

    public void checkErrorPopupVisible() {
        data.checkElementVisible(errorPopup);
    }

    public void popupDownloadButtonClick() {
        popupDownloadButton.click();
    }

    public void youtubeToMp3LinkClick() {
        youtubeToMp3Link.click();
    }

    public void youtubeToMp4LinkClick() {
        youtubeToMP4Link.click();
    }

    public void youtubeToMp4HDLinkClick() {
        youtubeToMP4HDLink.click();
    }

    public void youtubeToAviLinkClick() {
        youtubeToAviLink.click();
    }

    public void youtubeToAviHDLinkClick() {
        youtubeToAviHDLink.click();
    }
}
