package biz.flvto.website.flvtobiz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import biz.flvto.website.DataWorker;

public abstract class HeaderPlusFooter extends PageObject {

    protected DataWorker data;

    public HeaderPlusFooter(WebDriver driver) {
        super(driver);
        this.data = new DataWorker(driver);
    }

    //�����

    @FindBy(xpath = "//a[@title='FLVTO']")
    public WebElement mainPageLink;

    @FindBy(xpath = "//a[contains(@href,'/youtube-downloader/') and contains(@onclick,'Header')]")
    public WebElement desktopConverterPageLink;

    @FindBy(xpath = "//a[contains(@href,'/addon/') and contains(@onclick,'Header')]")
    public WebElement addonsPageLink;

    @FindBy(xpath = "//a[contains(@href,'/how-to/') and contains(@onclick,'Header')]")
    public WebElement howToPageLink;

    @FindBy(xpath = "//span[contains(@data-url,'/feedback/') and contains(@onclick,'Header')]")
    public WebElement supportPageLink;

    @FindBy(xpath = "//span[contains(@data-url,'/top100/') and contains(@onclick,'Header')]")
    public WebElement top100PageLink;

    @FindBy(css = "a.language-switcher")
    public WebElement languageSelector;

    @FindBy(css = "div.language-menu.lang")
    public WebElement oldLanguageSelector;

    @FindBy(css = "div.icon.menu")
    public WebElement mobileMenu;


    //social elements

    @FindBy(xpath = "//div[@class='fb-like fb_iframe_widget']")
    public WebElement facebookLike;

    @FindBy(id = "___plusone_0")
    public WebElement googlePlusLike;

    @FindBy(id = "twitter-widget-0")
    public WebElement twitterTweet;


    @FindBy(xpath = "//span[@class=' at4-icon aticon-facebook']")
    public WebElement facebookRightSideIcon;

    @FindBy(xpath = "//span[@class=' at4-icon aticon-twitter']")
    public WebElement twitterRightSideIcon;

    @FindBy(xpath = "//span[@class=' at4-icon aticon-google_plusone_share']")
    public WebElement googlePlusRightSideIcon;

    @FindBy(xpath = "//span[@class=' at4-icon aticon-pinterest_share']")
    public WebElement pinterestRightSideIcon;


    //�����

    @FindBy(css = "a.youtube-downloader-win")
    public WebElement downloadForWindowsPageLink;

    @FindBy(css = "a.youtube-downloader-mac")
    public WebElement downloadForMacPageLink;

    @FindBy(css = "a.youtube-player")
    public WebElement downloadForWindows10PageLink;

    @FindBy(css = "a.youtube-converter")
    public WebElement downloadForWindows8PageLink;


    @FindBy(xpath = "//a[contains(@href,'/terms/')]")
    public WebElement termsFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/policy/')]")
    public WebElement policyFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/faq/')]")
    public WebElement faqFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/feedback/') and contains(@onclick,'Footer')]")
    public WebElement feedbackFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/advertisers/')]")
    public WebElement advertisersFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/how-to/') and contains(@onclick,'Footer')]")
    public WebElement howToFooterLink;

    @FindBy(xpath = "//a[contains(@href,'/dmca/')]")
    public WebElement dmcaFooterLink;

    @FindBy(xpath = "//span[contains(@data-url,'/top100/') and contains(@onclick,'Footer')]")
    public WebElement top100FooterLink;

    @FindBy(xpath = "//a[contains(@href,'/addon/') and contains(@onclick,'Footer')]")
    public WebElement addonsFooterLink;


    public void mainPageLinkClick() {
        mainPageLink.click();
    }

    public void desktopConverterPageLinkClick() {
        desktopConverterPageLink.click();
    }

    public void addonsPageHeaderLinkClick() {
        addonsPageLink.click();
    }

    public void howToPageHeaderLinkClick() {
        howToPageLink.click();
    }

    public void supportPageLinkClick() {
        supportPageLink.click();
    }

    public void top100PageHeaderLinkClick() {
        top100PageLink.click();
    }

    public void downloadForWindowsPageLinkClick() {
        downloadForWindowsPageLink.click();
    }

    public void downloadForMacPageLinkClick() {
        downloadForMacPageLink.click();
    }

    public void downloadForWindows10PageLinkClick() {
        downloadForWindows10PageLink.click();
    }

    public void downloadForWindows8PageLinkClick() {
        downloadForWindows8PageLink.click();
    }


    public void termsFooterLinkClick() {
        termsFooterLink.click();
    }

    public void policyFooterLinkClick() {
        policyFooterLink.click();
    }

    public void faqFooterLinkClick() {
        faqFooterLink.click();
    }

    public void feedbackFooterLinkClick() {
        feedbackFooterLink.click();
    }

    public void advertisersFooterLinkClick() {
        advertisersFooterLink.click();
    }

    public void howToFooterLinkClick() {
        howToFooterLink.click();
    }

    public void dmcaFooterLinkClick() {
        dmcaFooterLink.click();
    }

    public void top100FooterLinkClick() {
        top100FooterLink.click();
    }

    public void addonsFooterLinkClick() {
        addonsFooterLink.click();
    }

    public void waitForPageChanging(String startUrl) {
        data.waitingForPageChanged(startUrl);
    }

    public void languageSelectorClick(boolean oldDesign) {
        if (oldDesign) {
            oldLanguageSelector.click();
        } else {
            languageSelector.click();
        }
    }

    public void selectLanguage(String lang) {
        String locator = "span." + lang;
        WebElement link = getDriver().findElement(By.cssSelector(locator));
        data.waitForElement(link);
        link.click();
    }

    public void mobileMenuClick() {
        mobileMenu.click();
    }

}
