package biz.flvto.website.flvtobiz;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Set;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/top100/"})
public class Top100 extends HeaderPlusFooter {

    private static final String desktopDataZones[] = {"812", "9", "6", "150", "168"};
    private static final String mobileDataZones[] = {};

    public Top100(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='fb-like-box fb_iframe_widget']")
    public WebElement facebookLikeBox;

    @FindBy(xpath = "//a[contains(@href,'convert')]")
    public WebElement convertLink;

    @FindBy(xpath = "//div[@class='current-format-value']")
    public WebElement currentFormatField;

    @FindBy(xpath = "//a[@class='title']")
    public WebElement firstVideoTitle;

    @FindBy(xpath = "//div[contains(@class,'current-format')]")
    public WebElement showFormatButton;

    @FindBy(xpath = "//*[@data-value='1']")
    public WebElement mp3SelectButton;

    @FindBy(xpath = "//*[@data-value='8']")
    public WebElement mp4SelectButton;

    @FindBy(xpath = "//*[@data-value='7']")
    public WebElement mp4HDSelectButton;

    @FindBy(xpath = "//*[@data-value='5']")
    public WebElement aviSelectButton;

    @FindBy(xpath = "//*[@data-value='9']")
    public WebElement aviHDSelectButton;


    public void checkDesktopBanners() {
        if (desktopDataZones.length > 0) {
            for (String zone : desktopDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public void checkMobileBanners() {
        if (mobileDataZones.length > 0) {
            for (String zone : mobileDataZones) {
                data.checkBanner(zone);
            }
        }
    }

    public String getFirstVideoTitle() {
        return firstVideoTitle.getText();
    }

    public void showFormatButtonClick() {
        boolean flag = true;
        while (flag) {
            showFormatButton.click();
            try {
                data.waitForElement(mp4SelectButton);
                flag = false;
            } catch (ElementNotVisibleException e) {

            }
        }
    }

    public void convertLinkClick() {
        convertLink.click();
    }

    public void mp3SelectButtonClick() {
        data.waitForElement(mp3SelectButton);
        mp3SelectButton.click();
    }

    public void mp4SelectButtonClick() {
        data.waitForElement(mp4SelectButton);
        mp4SelectButton.click();
    }

    public void mp4HDSelectButtonClick() {
        data.waitForElement(mp4HDSelectButton);
        mp4HDSelectButton.click();
    }

    public void aviSelectButtonClick() {
        data.waitForElement(aviSelectButton);
        aviSelectButton.click();
    }

    public void aviHDSelectButtonClick() {
        data.waitForElement(aviHDSelectButton);
        aviHDSelectButton.click();
    }

    public void checkCurrentFormatIs(String format) {
        String currentFormat = currentFormatField.getText();
        assertThat(currentFormat, is(format));
    }

    public void checkShowFormatButtonVisible() {
        data.checkElementVisible(showFormatButton);
    }

    public void switchToProgressWindow(Set<String> oldWindowsSet) {
        data.switchToNewWindow(oldWindowsSet);
    }

}
