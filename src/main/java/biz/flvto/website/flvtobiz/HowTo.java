package biz.flvto.website.flvtobiz;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.thucydides.core.annotations.At;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@At(urls = {"#HOST/*.*/how-to/"})
public class HowTo extends HeaderPlusFooter {

    public HowTo(WebDriver driver) {
        super(driver);
    }

    String linksXpath = "//ul[@class='nav-markers']/*/a";
    public List<WebElement> howtoLinks = new ArrayList<>();


    public void checkLinksCounter(int counter) {
        howtoLinks = getDriver().findElements(By.xpath(linksXpath));
        assertThat(howtoLinks.size(), is(counter));
    }

}
