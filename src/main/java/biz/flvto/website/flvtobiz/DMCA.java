package biz.flvto.website.flvtobiz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;


@At(urls = {"#HOST/*.*/dmca/"})
public class DMCA extends HeaderPlusFooter {

    public DMCA(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "dmca_name")
    public WebElement dmcaNameInput;

    @FindBy(id = "dmca_email")
    public WebElement dmcaEmailInput;

    @FindBy(id = "dmca_company")
    public WebElement dmcaCompanyInput;

    @FindBy(id = "dmca_address")
    public WebElement dmcaAddressInput;

    @FindBy(id = "dmca_url")
    public WebElement dmcaUrlInput;

    @FindBy(id = "dmca_description")
    public WebElement dmcaDescriptionTextarea;

    @FindBy(id = "dmca_captcha")
    public WebElement captchaInput;

    @FindBy(xpath = "//div[@class='for-captcha']/img")
    public WebElement captchaImage;

    @FindBy(id = "dmca_form_send")
    public WebElement sendButton;


    public void checkSendButtonVisible() {
        data.checkElementVisible(sendButton);
    }

}
