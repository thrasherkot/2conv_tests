package biz.flvto.website.flvtobiz;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;


@At(urls = {"#HOST/*.*/faq/"})
public class FAQ extends HeaderPlusFooter {

    public FAQ(WebDriver driver) {
        super(driver);
    }

    String headersXpath = "//h3";
    public List<WebElement> h3List = new ArrayList<>();


    @FindBy(xpath = "//a[text()='install YouTube Converter']")
    public WebElement installConverterLink;

    @FindBy(xpath = "//a[text()='downloader software']")
    public WebElement downloaderLink;

    public void checkHeadersCounter(int counter) {
        h3List = getDriver().findElements(By.xpath(headersXpath));
        assertThat(h3List.size(), is(counter));
    }

    public void installConverterLinkClick() {
        installConverterLink.click();
    }

    public void downloaderLinkClick() {
        downloaderLink.click();
    }


}
