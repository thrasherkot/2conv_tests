package biz.flvto.website.flvtobiz;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/feedback/"})
public class Support extends HeaderPlusFooter {

    public Support(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "feedback_name")
    public WebElement nameInput;

    @FindBy(id = "feedback_email")
    public WebElement emailInput;

    @FindBy(xpath = "//button[contains(@class,'multiselect')]")
    public WebElement showFeedbackTypesButton;

    @FindBy(xpath = "//button[contains(@class,'multiselect')]/span[2]")
    public WebElement currentFeedbackTypeField;

    String feedBackTypesXpath = "//label[@for]/span";

    public List<WebElement> feedbackTypeList = new ArrayList<>();

    @FindBy(id = "feedback_sourceUrl")
    public WebElement sourceUrlInput;

    @FindBy(id = "feedback_message")
    public WebElement feedbackMesageTextarea;

    @FindBy(id = "feedback_captcha")
    public WebElement captchaInput;

    @FindBy(xpath = "//div[@class='for-captcha']/img")
    public WebElement captchaImage;

    @FindBy(id = "feedback_form_send")
    public WebElement sendFeedbackButton;


    public void typeName(String name) {
        typeInto(nameInput, name);
    }

    public void typeEmail(String email) {
        typeInto(emailInput, email);
    }

    public void showFeedbackTypesButtonClick() {
        showFeedbackTypesButton.click();
    }

    public void selectFeedbackType(String newType) {
        feedbackTypeList = getDriver().findElements(By.xpath(feedBackTypesXpath));
        for (WebElement type : feedbackTypeList) {
            if (type.getText().equals(newType)) {
                type.click();
                break;
            }
        }
    }

    public void typeSourceUrl(String url) {
        typeInto(sourceUrlInput, url);
    }

    public void typeFeedbackMessage(String message) {
        typeInto(feedbackMesageTextarea, message);
    }

    public String getCurrentType() {
        return currentFeedbackTypeField.getText();
    }

    public void isSendButtonVisible() {
        data.checkElementVisible(sendFeedbackButton);
    }

}
