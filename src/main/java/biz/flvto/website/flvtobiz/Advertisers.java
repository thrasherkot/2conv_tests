package biz.flvto.website.flvtobiz;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;


@At(urls = {"#HOST/*.*/advertisers/"})
public class Advertisers extends HeaderPlusFooter {

    public Advertisers(WebDriver driver) {
        super(driver);
    }


    @FindBy(id = "advertisers_name")
    public WebElement advertiserNameInput;

    @FindBy(id = "advertisers_email")
    public WebElement advertiserEmailInput;

    @FindBy(id = "advertisers_site")
    public WebElement advertiserWebsiteInput;

    @FindBy(id = "advertisers_skype")
    public WebElement advertiserSkypeInput;

    @FindBy(id = "advertisers_usCpm")
    public WebElement usCPMInput;

    @FindBy(id = "advertisers_geo")
    public WebElement geolocationInput;

    @FindBy(id = "advertisers_cpm")
    public WebElement cPMOfferInput;

    @FindBy(id = "advertisers_message")
    public WebElement messageTextarea;

    @FindBy(id = "advertisers_captcha")
    public WebElement captchaInput;

    @FindBy(xpath = "//div[@class='for-captcha']/img")
    public WebElement captchaImage;

    @FindBy(id = "advertisers_form_send")
    public WebElement sendButton;


    public void checkSendButtonVisible() {
        data.checkElementVisible(sendButton);
    }


}
