package biz.flvto.website.flvtobiz;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;
import biz.flvto.requirements.ParametersList;


@At(urls = {"#HOST/*.*/youtube-downloader/",
        "#HOST/*.*/youtube-downloader-for-mac/*.*"})
public class DesktopConverterPage extends HeaderPlusFooter {

    public DesktopConverterPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@onclick='sendAnalytics(this)']")
    public WebElement downloadButton;

    @FindBy(xpath = "//a[text()='Convert another video']")
    public WebElement convertAnotherVideoTabLink;

    @FindBy(xpath = "//div[@class='desktop-download']//a[contains(@href,'/youtube-downloader-for-mac/')]")
    public WebElement downloadAppForMacLink;

    @FindBy(xpath = "//a[@href='/youtube-video-downloader/']")
    public WebElement mirrorOneLink;

    @FindBy(xpath = "//a[@href='/youtube-music-downloader/']")
    public WebElement mirrorTwoLink;

    @FindBy(xpath = "//a[@href='/youtube-song-downloader/']")
    public WebElement mirrorThreeLink;

    @FindBy(xpath = "//a[@href='/youtube-video-to-mp3/']")
    public WebElement mirrorFourLink;

    @FindBy(xpath = "//a[@href='/youtube-music-converter/']")
    public WebElement mirrorFiveLink;

    @FindBy(css = "div.popup")
    public WebElement modalDialog;

    @FindBy(xpath = "//span[@class='button' and contains(@onclick,'Landing Popup')]")
    public WebElement downloadPopupButton;

    @FindBy(xpath = "//span[@class='popup-close close']")
    public WebElement closePopupButton;

    @FindBy(xpath = "//span[@class='converter button store']")
    public WebElement win8_landing_download_button;


    public void checkDownloadButtonVisible() {
        data.checkElementVisible(downloadButton);
    }

    public void checkModalDialogVisible() {
        data.checkElementVisible(modalDialog);
    }

    public void switchToMicrosoftWindow(Set<String> oldWindowsSet) {
        data.switchToNewWindow(oldWindowsSet);
    }

    public void downloadAppButtonClick() {
        downloadButton.click();
    }

    public void downloadAppForMacLinkClick() {
        downloadAppForMacLink.click();
    }

    public void downloadPopupButtonClick() {
        try {
            data.waitForElement(downloadPopupButton);
        } catch (StaleElementReferenceException e) {

        }
        downloadPopupButton.click();
    }

    public void closePopupButtonClick() {
        try {
            element(closePopupButton).waitUntilClickable();
        } catch (StaleElementReferenceException e) {

        }
        closePopupButton.click();
    }

    public void verifyFileChecksumm(String os) {
        FileInputStream inFile;
        try {
            File downloadPath = new File(ParametersList.downloadPath);
            String downloadedFile[] = downloadPath.list();
            inFile = new FileInputStream(downloadedFile[0]);
            String md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(inFile);
            if (os.equals("win")) {
                assertThat(md5, is(ParametersList.md5ExeFile));
            } else if (os.equals("mac")) {
                assertThat(md5, is(ParametersList.md5DmgFile));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void waitForPageLoad() {
        new WebDriverWait(getDriver(), 10).until(ExpectedConditions.elementToBeClickable(downloadButton));
    }

    public void checkForWin10LandingDownloadButton() {
        data.checkElementVisible(win8_landing_download_button);
    }
}
