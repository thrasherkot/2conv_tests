package biz.flvto.website.flvtobiz;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;

@At(urls = {"#HOST/*.*/policy/"})
public class PrivacyPolicy extends HeaderPlusFooter {

    public PrivacyPolicy(WebDriver driver) {
        super(driver);
    }


    String headersXpath = "//h1";
    public List<WebElement> h2List = new ArrayList<>();


    @FindBy(xpath = "//a[text()='http://www.flvto.biz/feedback/']")
    public WebElement goToFeedbackLink;

    public void checkHeadersCounter(int counter) {
        h2List = getDriver().findElements(By.xpath(headersXpath));
        assertThat(h2List.size(), is(counter));
    }


    public void goToFeedbackLinkClick() {
        goToFeedbackLink.click();
    }

}
