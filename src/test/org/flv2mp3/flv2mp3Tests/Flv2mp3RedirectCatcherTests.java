package org.flv2mp3.flv2mp3Tests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithDriver;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import biz.flvto.requirements.*;
import biz.flvto.steps.Flv2Mp3Steps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.Flv2mp3Org.RedirectTest.class)
@WithTag("ghostbusters")
@UseTestDataFrom(value = "src/website/userAgent.csv")
public class Flv2mp3RedirectCatcherTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlv2Mp3)
    public Pages pages;

    @Steps
    public Flv2Mp3Steps endUser;


    private String device, ua, widthStr, heightStr;
    private boolean isMobile = false;

    @Qualifier
    public String qualifier() {
        return device + "=>" + ua + "=>" + widthStr + "=>" + heightStr;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public void setWidth(String width) {
        this.widthStr = width;
    }

    public void setHeight(String height) {
        this.heightStr = height;
    }

    @Before
    public void setUserAgent() {
        ParametersList.setUserAgent("ua");
        if (device.equals("g531f") || device.equals("galaxy s6") || device.equals("iphone 6") || device.equals("ipad")) {
            isMobile = true;
        }
    }

    @Test
    @WithDriver("provided")
    @WithTags({
            @WithTag("org/flv2mp3"),
            @WithTag("catcher")
    })
    public void check_index_redirect() {
        endUser.is_main_page_open(isMobile);
        endUser.wait_a_minute();
        endUser.check_redirect();

    }

    @Test
    @WithTags({
            @WithTag("org/flv2mp3"),
            @WithTag("catcher")
    })
    public void progress_catcher() {
        endUser.is_main_page_open(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.stop_convertion();
        endUser.wait_a_minute();
        endUser.check_redirect();
    }

    @Test
    @WithTags({
            @WithTag("org/flv2mp3"),
            @WithTag("catcher")
    })
    public void download_catcher() {
        endUser.is_main_page_open(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.download_converted_video_button_click();
        endUser.wait_a_minute();
        endUser.check_redirect();
    }


}
