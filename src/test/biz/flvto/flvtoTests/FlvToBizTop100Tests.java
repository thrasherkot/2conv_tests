package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.ConverterFunctional.class)
@WithTag("FlvTo")
public class FlvToBizTop100Tests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Top100"),
            @WithTag("MFlvTo")
    })
    public void convert_from_top100_to_mp3() {
        endUser.top100_page_open();
        endUser.set_window_size(isMobile);
        endUser.is_top100_page_open();
        endUser.related_video_convert("mp3");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Top100"),
            @WithTag("MFlvTo")
    })
    public void convert_from_top100_to_mp4() {
        endUser.top100_page_open();
        endUser.set_window_size(isMobile);
        endUser.is_top100_page_open();
        endUser.related_video_convert("mp4");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Top100"),
            @WithTag("MFlvTo")
    })
    public void convert_from_top100_to_mp4_hd() {
        endUser.top100_page_open();
        endUser.set_window_size(isMobile);
        endUser.is_top100_page_open();
        endUser.related_video_convert("mp4_hd");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Top100"),
            @WithTag("MFlvTo")
    })
    public void convert_from_top100_to_avi() {
        endUser.top100_page_open();
        endUser.set_window_size(isMobile);
        endUser.is_top100_page_open();
        endUser.related_video_convert("avi");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }


    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Top100"),
            @WithTag("MFlvTo")
    })
    public void convert_from_top100_to_avi_hd() {
        endUser.top100_page_open();
        endUser.set_window_size(isMobile);
        endUser.is_top100_page_open();
        endUser.related_video_convert("avi_hd");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

}
