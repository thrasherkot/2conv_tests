package biz.flvto.flvtoTests;

import net.thucydides.core.annotations.*;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.RobotTxtTest.class)
@WithTag("FlvTo")
public class FlvToBizRobotsTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }


    @Test
    @WithTags({
            @WithTag("Robots")
    })
    public void check_actuality_robots_txt() {
        endUser.robots_file_open();
        endUser.is_robots_file_open();
        endUser.check_robots_file();
    }


}
