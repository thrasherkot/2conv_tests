package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.MirrorsTest.class)
@WithTag("FlvTo")
public class FlvToBizMirrorsTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_mp3_mirror_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_current_url_before_click();
        endUser.youtube_to_mp3_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_current_url("youtube-to-mp3/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_mp4_mirror_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_current_url_before_click();
        endUser.youtube_to_mp4_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_current_url("youtube-to-mp4/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_mp4_hd_mirror_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_current_url_before_click();
        endUser.youtube_to_mp4_hd_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_current_url("youtube-to-mp4-hd/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_avi_mirror_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_current_url_before_click();
        endUser.youtube_to_avi_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_current_url("youtube-to-avi/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_avi_hd_mirror_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_current_url_before_click();
        endUser.youtube_to_avi_hd_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_current_url("youtube-to-avi-hd/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_youtube_to_mp3_high_quality_mirror_works() {
        endUser.open_page("youtube-to-mp3-high-quality/");
        endUser.maximize_window();
        endUser.check_index_mirror_with_locale("youtube-to-mp3-high-quality/");
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_download_dailymotion_mirror_works() {
        endUser.open_page("download-dailymotion-videos/");
        endUser.maximize_window();
        endUser.check_index_mirror_with_locale("download-dailymotion-videos/");
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("mirror")
    })
    public void is_soundcloud_to_mp3_mirror_works() {
        endUser.open_page("soundcloud-to-mp3/");
        endUser.maximize_window();
        endUser.check_index_mirror_with_locale("soundcloud-to-mp3/");
    }

}
