package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.FlvToBiz.RedirectTest.class)
@WithTag("ghostbusters")
@UseTestDataFrom(value = "src/website/userAgent.csv")
public class FlvToRedirectCatcherTests {

    private String device, ua, widthStr, heightStr;
    private int width, height;

    @Qualifier
    public String qualifier() {
        return device + "=>" + ua + "=>" + widthStr + "=>" + heightStr;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public void setUa(String ua) {
        this.ua = ua;
    }

    public void setWidth(String width) {
        this.widthStr = width;
    }

    public void setHeight(String height) {
        this.heightStr = height;
    }

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @Before
    public void setupProfile() {
        ParametersList.setUserAgent(ua);
        width = Integer.parseInt(widthStr);
        height = Integer.parseInt(heightStr);
    }

    @Test
    @WithTags({
            @WithTag("catcher")

    })
    public void index_catcher() {
//		endUser.start_request_capture("index");
        endUser.is_main_page_open();
        endUser.set_window_size(width, height);
        endUser.wait_a_minute();
        endUser.check_redirect();
    }

    @Test
    @WithTags({
            @WithTag("catcher")
    })
    public void progress_catcher() {
        endUser.is_main_page_open();
        endUser.set_window_size(width, height);
        endUser.type_valid_youtube_link();
//		endUser.start_request_capture("progress");
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.stop_convertion();
        endUser.wait_a_minute();
        endUser.check_redirect();
    }

    @Test
    @WithTags({
            @WithTag("catcher")
    })
    public void download_catcher() {
        endUser.is_main_page_open();
        endUser.set_window_size(width, height);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
//		endUser.start_request_capture("download");
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.download_converted_video_button_click();
        endUser.wait_a_minute();
        endUser.check_redirect();
    }

}
