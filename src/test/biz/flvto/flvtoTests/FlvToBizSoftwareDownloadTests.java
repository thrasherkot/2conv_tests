package biz.flvto.flvtoTests;

import java.io.File;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

import net.serenitybdd.core.Serenity;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.DownloadSoftwareTest.class)
public class FlvToBizSoftwareDownloadTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Before
    public void setupProfile() {

        ProfilesIni allProfiles = new ProfilesIni();
        FirefoxProfile myProfile = allProfiles.getProfile("SeleniumDownload");
        Serenity.useFirefoxProfile(myProfile);

        File downloadPath = new File(ParametersList.downloadPath);
        deletefile(downloadPath);
        downloadPath.mkdir();


    }


    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_index_dropdown() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_windows_handles();
        endUser.show_formats_button_click();
        endUser.download_app_index_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }


    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_index_popup() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.get_windows_handles();
        endUser.convert_button_click();
        endUser.index_popup_download_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }

    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_progress_popup() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.type_deleted_video_url();
        endUser.convert_button_click();
        endUser.wait_for_page_change();
        endUser.is_progress_page_open();
        endUser.get_windows_handles();
        endUser.wait_for_progress_error_popup();
        endUser.progress_popup_download_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }

    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_download_page() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.get_windows_handles();
        endUser.download_using_app_result_page_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }

    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_page_404() {
        endUser.get_404_page();
        endUser.is_404_page_open();
        endUser.page_404_download_button_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }

    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_landing_popup() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.show_formats_button_click();
        endUser.download_converter_tips_button_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_for_mac_link_click();
        endUser.wait_for_page_change();
        endUser.get_windows_handles();
        endUser.download_popup_button_desktop_converter_page_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("win");
    }


    @Test
    @WithTags({
            @WithTag("Download")
    })
    public void download_from_mac_landing() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.show_formats_button_click();
        endUser.download_converter_tips_button_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.download_app_for_mac_link_click();
        endUser.wait_for_page_change();
        endUser.close_popup_at_desktop_converter_page();
        endUser.download_app_button_click();
        endUser.wait_for_file_download();
        endUser.check_the_downloaded_file("mac");
    }

    private void deletefile(File path) {
        if (path.isDirectory()) {
            for (File f : path.listFiles()) {
                if (f.isDirectory()) deletefile(f);
                else f.delete();
            }
        }
        path.delete();
    }


}
