package biz.flvto.flvtoTests;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.TestData;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.FlvToBiz.ValidationTest.class)
@WithTags({
        @WithTag("Convertation"),
        @WithTag("validation"),
        @WithTag("FlvTo")
})

public class FlvToBizValiadtionTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @TestData
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                ParametersList.shouldBeValidUrls);
    }

    private final String testUrl;

    public FlvToBizValiadtionTests(String url) {
        this.testUrl = url;
    }

    @Test
    public void validation_youtube_urls() {
        endUser.is_main_page_open();
        endUser.validate_url(testUrl);

    }
}
