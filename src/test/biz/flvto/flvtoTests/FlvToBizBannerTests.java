package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithDriver;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.BannerTest.class)

@WithTag("FlvTo")
public class FlvToBizBannerTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }


    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("MFlvTo")
    })
    public void main_page_banner_test() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.check_main_page_banners_with_locales();
    }

    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("MFlvTo")
    })
    public void top100_page_banner_test() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.top100_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_top100_page_open();
        endUser.check_top100_page_banners_with_locales();
    }

    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("MFlvTo")
    })
    public void download_page_banner_test() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.check_download_page_banners_with_locales();
    }

    @Test
    @WithDriver("provided")
    @WithTags({
            @WithTag("Banner"),
            @WithTag("softbanner")
    })
    public void software_ads_page_test() {
        endUser.is_main_page_open();
        endUser.software_ads_page_open();
        endUser.check_software_ads_banner();
    }

}
