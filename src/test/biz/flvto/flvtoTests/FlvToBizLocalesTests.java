package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.FlvToBiz.LocalesTest.class)
@UseTestDataFrom(value = "src/website/local.csv")
@WithTag("FlvTo")
public class FlvToBizLocalesTests {

    private String locale, path;

    @Qualifier
    public String qualifier() {
        return locale + "=>" + path;
    }

    public void setLocaleName(String name) {
        this.locale = name;
    }

    public void setLocalPath(String path) {
        this.path = path;
    }

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }


    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("MFlvTo")
    })
    public void progress_page_banner_test() {
        endUser.is_main_page_with_locale_open(locale, path);
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.stop_convertion();
        endUser.check_progress_page_banners();
    }

}
