package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.LinksTest.class)
@WithTag("FlvTo")
public class FlvToBizLinksTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("landing")
    })
    public void is_win8_landing_works() {
        endUser.win10_landing_open();
        endUser.check_win10_landing_open();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links")
    })
    public void is_download_converter_tips_button_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.show_formats_button_click();
        endUser.download_converter_tips_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.check_desktop_converter_page_elements_visible();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer")
    })
    public void is_download_app_for_windows_footer_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.download_app_for_windows_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.check_desktop_converter_page_elements_visible();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer")
    })
    public void is_download_app_for_mac_footer_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.download_app_for_mac_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.check_desktop_converter_page_elements_visible();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("microsoft")
    })
    public void is_download_app_for_win10_footer_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.download_app_for_win10_footer_link_click();
        endUser.is_microsoft_store_page_open();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("microsoft")
    })
    public void is_download_app_for_win8_footer_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.download_app_for_win8_footer_link_click();
        endUser.is_microsoft_store_page_open();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("header")
    })
    public void is_desktop_converter_header_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.desktop_converter_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_desktop_converter_page_open();
        endUser.check_desktop_converter_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("header"),
            @WithTag("MFlvTo")
    })
    public void is_top100_header_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.top100_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_top100_page_open();
        endUser.check_elements_at_top100_page_visible_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("header")
    })
    public void is_addons_header_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.addons_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_addons_page_open();
        endUser.check_elements_at_addons_page_availible_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("header"),
            @WithTag("MFlvTo")
    })
    public void is_how_to_header_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.how_to_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_how_to_page_open();
        endUser.check_elements_at_how_to_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("header"),
            @WithTag("MFlvTo")
    })
    public void is_support_header_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.support_header_link_click();
        endUser.wait_for_page_change();
        endUser.is_support_page_open();
        endUser.check_elements_at_support_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_terms_of_service_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.terms_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_terms_of_service_page_open();
        endUser.check_elements_at_terms_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_how_to_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.how_to_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_how_to_page_open();
        endUser.check_elements_at_how_to_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_privacy_policy_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.privacy_policy_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_privacy_policy_page_open();
        endUser.check_elements_at_privacy_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_dmca_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.dmca_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_dmca_page_open();
        endUser.check_elements_at_dmca_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_feedback_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.feedback_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_support_page_open();
        endUser.check_elements_at_support_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_faq_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.faq_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_faq_page_open();
        endUser.check_elements_at_faq_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_advertisers_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.advertisers_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_advertisers_page_open();
        endUser.check_elements_at_advertisers_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer"),
            @WithTag("MFlvTo")
    })
    public void is_top100_footer_link_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.top100_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_top100_page_open();
        endUser.check_elements_at_top100_page_visible_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("footer")
    })
    public void is_addons_footer_link_works() {
        endUser.is_main_page_open();
        endUser.maximize_window();
        endUser.addons_footer_link_click();
        endUser.wait_for_page_change();
        endUser.is_addons_page_open();
        endUser.check_elements_at_addons_page_availible_with_locale();
    }


    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("install")
    })
    public void is_youtube_downloader_install_link_works() {
        endUser.open_page("youtube-downloader/install/");
        endUser.maximize_window();
        endUser.check_page_with_locale("youtube-downloader/install/");
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("install")
    })
    public void install_framework_35_link_works() {
        endUser.open_page("software/install-framework3.5/");
        endUser.maximize_window();
        endUser.check_page_from_software("software/install-framework3.5/");
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("install")
    })
    public void install_framework_40_link_works() {
        endUser.open_page("software/install-framework4.0/");
        endUser.maximize_window();
        endUser.check_page_from_software("software/install-framework4.0/");
    }

    @Test
    @WithTags({
            @WithTag("FlvTo Links"),
            @WithTag("install")
    })
    public void is_youtube_downloader_install_finish_link_works() {
        endUser.open_page("youtube-downloader/install-finish/");
        endUser.maximize_window();
        endUser.check_page_from_software("youtube-downloader/install-finish/");
    }


}
