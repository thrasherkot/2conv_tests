package biz.flvto.flvtoTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityRunner.class)
@Story(Application.FlvToBiz.ConverterFunctional.class)
@WithTags({
        @WithTag("FlvTo"),
        @WithTag("MFlvTo")
})
public class FlvToBizDownloadsPageTests {


    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("Related"),
            @WithTag("Download Page")
    })
    public void convert_from_related_donwnload_to_mp3() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.related_video_convert("mp3");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Related"),
            @WithTag("Download Page")
    })
    public void convert_from_related_donwnload_to_mp4() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.related_video_convert("mp4");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Related"),
            @WithTag("Download Page")
    })
    public void convert_from_related_donwnload_to_mp4_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.related_video_convert("mp4_hd");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Related"),
            @WithTag("Download Page")
    })
    public void convert_from_related_donwnload_to_avi() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.related_video_convert("avi");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Related"),
            @WithTag("Download Page")
    })
    public void convert_from_related_donwnload_to_avi_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.related_video_convert("avi_hd");
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Download Page"),
            @WithTag("email")
    })
    public void send_to_email_from_download_page() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();

        endUser.send_link_to_email_click();
        endUser.type_email_to_send();
        endUser.set_minute_for_waits();
        endUser.send_email_button_click();
        endUser.check_email_from_download_sended();
    }


}
