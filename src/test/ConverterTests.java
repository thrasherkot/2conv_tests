import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithDriver;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.TestData;
import biz.flvto.requirements.Application;
import biz.flvto.requirements.ParametersList;
import biz.flvto.steps.FlvToSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.Api.Coverter.class)
@WithTag("Converter")
public class ConverterTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrlFlvto)
    public Pages pages;

    @Steps
    public FlvToSteps endUser;

    @TestData
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                ParametersList.urlForConverter);
    }

    private final String url, format;

    public ConverterTests(String url, String format) {
        this.url = url;
        this.format = format;
    }


    @Test
    @WithDriver("provided")
    @WithTag("Real convertion")
    public void check_real_convertion() {
        endUser.is_main_page_open();
        endUser.select_format(format);
        endUser.check_format_selected(format);
        endUser.type_youtube_link(url);
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.check_direct_link();
    }

    @Test
    @WithDriver("provided")
    @WithTag("Convertion from cache")
    public void check_convertion_from_cache() {
        endUser.is_main_page_open();
        endUser.select_format(format);
        endUser.check_format_selected(format);
        endUser.type_youtube_link(url);
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.check_direct_link();
    }

}
