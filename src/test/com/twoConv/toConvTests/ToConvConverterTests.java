package com.twoConv.toConvTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityRunner.class)
@Story(Application.ToConvCom.ConverterFunctional.class)
@WithTags({
        @WithTag("2conv"),
        @WithTag("M2conv")
})
public class ToConvConverterTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }


    @Test
    @WithTags({
            @WithTag("Convertion")
    })
    public void is_convert_to_mp3_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.select_mp3_format();
        endUser.check_mp3_format_selected();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion")
    })
    public void is_convert_to_mp4_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.select_mp4_format();
        endUser.check_mp4_format_selected();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion")
    })
    public void is_convert_to_mp4_hd_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.select_mp4_hd_format();
        endUser.check_mp4_hd_format_selected();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion")
    })
    public void is_convert_to_avi_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.select_avi_format();
        endUser.check_avi_format_selected();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion")
    })
    public void is_convert_to_avi_hd_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.select_avi_hd_format();
        endUser.check_avi_hd_format_selected();
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Progress")
    })
    public void convert_from_progress_related_to_mp3() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp3_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Progress")
    })
    public void convert_from_progress_related_to_mp4() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp4_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Progress")
    })
    public void convert_from_progress_related_to_mp4_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp4_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Progress")
    })
    public void convert_from_progress_related_to_avi() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.related_show_format_button_click();
        endUser.related_avi_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }


    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Progress")
    })
    public void convert_from_progress_related_to_avi_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.related_show_format_button_click();
        endUser.related_avi_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_main_to_mp3() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.switch_to_related_frame();
        endUser.related_show_format_button_click();
        endUser.related_mp3_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_main_to_mp4() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.switch_to_related_frame();
        endUser.related_show_format_button_click();
        endUser.related_mp4_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_main_to_mp4_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.switch_to_related_frame();
        endUser.related_show_format_button_click();
        endUser.related_mp4_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_main_to_avi() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.switch_to_related_frame();
        endUser.related_show_format_button_click();
        endUser.related_avi_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_main_to_avi_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.switch_to_related_frame();
        endUser.related_show_format_button_click();
        endUser.related_avi_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_donwnload_to_mp3() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp3_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_donwnload_to_mp4() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp4_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_donwnload_to_mp4_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.related_show_format_button_click();
        endUser.related_mp4_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_donwnload_to_avi() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.related_show_format_button_click();
        endUser.related_avi_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

    @Test
    @WithTags({
            @WithTag("Convertion"),
            @WithTag("Related")
    })
    public void convert_from_related_donwnload_to_avi_hd() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.related_show_format_button_click();
        endUser.related_avi_hd_format_select();
        endUser.related_convert_button_click();
        endUser.switch_to_new_window();
        endUser.is_progress_page_open();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
    }

}
