package com.twoConv.toConvTests;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.TestData;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.ToConvCom.ValidationTest.class)
@WithTag("2conv")
public class ToConvValidationTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;


    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @TestData
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                ParametersList.shouldBeValidUrls);
    }

    private final String testUrl;

    public ToConvValidationTests(String url) {
        this.testUrl = url;
    }

    @Test
    public void validation_youtube_urls() {
        endUser.is_main_page_open();
        endUser.validate_url(testUrl);

    }

}
