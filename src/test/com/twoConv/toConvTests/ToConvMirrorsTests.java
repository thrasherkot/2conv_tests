package com.twoConv.toConvTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityRunner.class)
@Story(Application.ToConvCom.MirrorsTest.class)
@WithTags({
        @WithTag("2conv"),
        @WithTag("M2conv")
})
public class ToConvMirrorsTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("mirror")
    })
    public void is_youtube_video_downloader_mirror_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.get_current_url_before_click();
        endUser.youtube_video_downloader_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_mirror_url("youtube-video-downloader/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("mirror")
    })
    public void is_youtube_to_avi_mirror_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.get_current_url_before_click();
        endUser.youtube_to_avi_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_mirror_url("youtube-avi/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("mirror")
    })
    public void is_youtube_to_mp3_mirror_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.get_current_url_before_click();
        endUser.youtube_to_mp3_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_mirror_url("youtube-mp3/");
        endUser.check_main_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("mirror")
    })
    public void is_youtube_to_mp4_mirror_works() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.get_current_url_before_click();
        endUser.youtube_to_mp4_mirror_link_click();
        endUser.wait_for_page_change();
        endUser.check_mirror_url("youtube-mp4/");
        endUser.check_main_page_banners();
    }

}
