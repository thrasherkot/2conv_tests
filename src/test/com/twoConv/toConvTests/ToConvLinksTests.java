package com.twoConv.toConvTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityRunner.class)
@Story(Application.ToConvCom.LinksTest.class)
@WithTag("2conv")
public class ToConvLinksTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("header")
    })
    public void is_desktop_converter_header_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.maximize_window();
        endUser.desktop_converter_header_link_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.check_desktop_converter_page_elements_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("header"),
            @WithTag("M2conv")
    })
    public void is_support_header_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.support_header_link_click();
        endUser.is_support_page_open();
        endUser.check_elements_at_support_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_terms_of_service_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.maximize_window();
        endUser.terms_footer_link_click();
        endUser.is_terms_of_service_page_open();
        endUser.check_info_page_header_with_locale("terms");
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_privacy_policy_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.privacy_policy_footer_link_click();
        endUser.is_privacy_policy_page_open();
        endUser.check_info_page_header_with_locale("policy");
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_report_abuse_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.report_abuse_footer_link_click();
        endUser.is_support_page_open();
        endUser.check_elements_at_support_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_feedback_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.feedback_footer_link_click();
        endUser.is_support_page_open();
        endUser.check_elements_at_support_page_with_locale();
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_faq_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.faq_footer_link_click();
        endUser.is_faq_page_open();
        endUser.check_info_page_header_with_locale("faq");
    }

    @Test
    @WithTags({
            @WithTag("footer"),
            @WithTag("M2conv")
    })
    public void is_advertisers_footer_link_works() {
        endUser.is_main_page_with_locale_open("Russian", "/ru/");
        endUser.set_window_size(isMobile);
        endUser.advertisers_footer_link_click();
        endUser.is_advertisers_page_open();
        endUser.check_elements_at_advertisers_page_with_locale();
    }

}
