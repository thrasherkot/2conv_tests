package com.twoConv.toConvTests;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityRunner.class)
@Story(Application.ToConvCom.RobotTxtTest.class)
@WithTag("2conv")
public class ToConvRobotsTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;


    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Test
    @WithTags({
            @WithTag("Robots")
    })
    public void check_actuality_robots_txt() {
        endUser.robots_file_open();
        endUser.is_robots_file_open();
        endUser.check_robots_file();
    }

}
