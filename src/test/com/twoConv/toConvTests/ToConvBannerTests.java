package com.twoConv.toConvTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityRunner.class)
@Story(Application.ToConvCom.BannerTest.class)
@WithTag("2conv")
public class ToConvBannerTests {

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;

    private static boolean isMobile = false;

    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
        isMobile = ParametersList.checkMobileTag();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }


    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("M2conv")
    })
    public void main_page_banner_test() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.check_main_page_banners_with_locale();
    }


    @Test
    @WithTags({
            @WithTag("Banner"),
            @WithTag("M2conv")
    })
    public void download_page_banner_test() {
        endUser.is_main_page_open();
        endUser.set_window_size(isMobile);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.wait_until_convertion_finish();
        endUser.is_download_page_open();
        endUser.check_download_page_banners_with_locale();
    }

}
