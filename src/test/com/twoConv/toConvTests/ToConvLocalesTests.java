package com.twoConv.toConvTests;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Story;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.core.annotations.WithTags;
import net.thucydides.core.pages.Pages;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import biz.flvto.requirements.*;
import biz.flvto.steps.ToConvSteps;

@RunWith(SerenityParameterizedRunner.class)
@Story(Application.ToConvCom.LocalesTest.class)
@WithTag("2conv")
@UseTestDataFrom(value = "src/website/local.csv")
public class ToConvLocalesTests {

    private String locale, path;

    @Qualifier
    public String qualifier() {
        return locale + "=>" + path;
    }

    public void setLocaleName(String name) {
        this.locale = name;
    }

    public void setLocalPath(String path) {
        this.path = path;
    }

    @Managed()
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = ParametersList.baseUrl2conv)
    public Pages pages;

    @Steps
    ToConvSteps endUser;


    @BeforeClass
    public static void setUp() {
        ParametersList.setUserAgent();
    }

    @Before
    public void setupProfile() {
        ParametersList.setProfile();
    }

    @Test
    @WithTags({
            @WithTag("Banner")
    })
    public void progress_page_banner_test() {
        endUser.is_main_page_with_locale_open(locale, path);
        endUser.type_valid_youtube_link();
        endUser.convert_button_click();
        endUser.is_progress_page_open();
        endUser.stop_convertion();
        endUser.check_progress_page_banners();
    }

    @Test
    @WithTags({
            @WithTag("button")
    })
    public void is_download_converter_button_works() {
        endUser.is_main_page_with_locale_open(locale, path);
        endUser.maximize_window();
        endUser.download_converter_button_click();
        endUser.switch_to_new_window();
        endUser.is_desktop_converter_page_open();
        endUser.check_download_button_at_desktop_converter_page_visible();
    }

}
